<nav id="mk-vm-menu" class="mk-vm-menuwrapper menu-hover-style-2 js-main-nav">
  <ul id="menu-main" class="mk-vm-menu">
    <li id="menu-item-16"
        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item">
      <a href="/bccamp.lt"><span class="meni-item-text">Homepage</span></a>
    </li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a
              href="/bccamp.lt/about.php"><span
                class="meni-item-text">About us</span></a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a
              href="#"><span
                class="meni-item-text">Coaches and personal</span></a></li>
    <li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page"><a
              href="/bccamp.lt/vidinis.php"><span
                class="meni-item-text">U14-U18 Camp</span></a></li>
    <li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page"><a
              href="/bccamp.lt/vidinis.php">
        <span class="meni-item-text">U8-U13 Camp</span>
      </a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a
              href="/bccamp.lt/registracija.php"><span
                class="meni-item-text">Prices and registration</span></a></li>
  </ul>
</nav>