<header data-height="90" data-sticky-height="55" data-responsive-height="90" data-transparent-skin=""
        data-header-style="4" data-sticky-style="fixed" data-sticky-offset="header" id="mk-header-1"
        class="mk-header header-style-4 header-align-left  toolbar-false menu-hover-2 sticky-style-fixed mk-background-stretch boxed-header "
        role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <div class="mk-header-holder">
        <div class="mk-header-inner">

            <div class="mk-header-bg "></div>


            <div class="mk-nav-responsive-link">
                <div class="mk-css-icon-menu">
                    <div class="mk-css-icon-menu-line-1"></div>
                    <div class="mk-css-icon-menu-line-2"></div>
                    <div class="mk-css-icon-menu-line-3"></div>
                </div>
            </div>
            <div class=" header-logo fit-logo-img add-header-height  ">

                <a href="/bccamp.lt"
                   title="BC Camp">

                    <img class="mk-desktop-logo dark-logo" title="BC Camp - krepšinio stovykla"
                         alt="BC Camp - krepšinio stovykla" src="./bccamp-logo.png">

                    <img class="mk-desktop-logo light-logo"
                         title="Another amazing template created by Jupiter WordPress theme"
                         alt="Another amazing template created by Jupiter WordPress theme"
                         src="./bccamp-logo.png">


                </a>
            </div>

            <div class="clearboth"></div>

            <?php include("includes/nav.php"); ?>
            <div class="mk-header-right">
                <div class="mk-header-social header-section show">
                    <div class="clearboth"></div>
                </div>


            </div>

            <div class="mk-responsive-wrap" style="max-height: 755px;">

                <nav class="menu-main-container">
                    <ul id="menu-main-1" class="mk-responsive-nav">
                        <li id="responsive-menu-item-16"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5 current_page_item">
                            <a class="menu-item-link js-smooth-scroll"
                               href="http://demos.artbees.net/jupiter5/fortuna/">Home</a></li>
                        <li id="responsive-menu-item-46"
                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                class="menu-item-link js-smooth-scroll"
                                href="http://demos.artbees.net/jupiter5/fortuna/about-us/">About Us</a></li>
                        <li id="responsive-menu-item-78"
                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                class="menu-item-link js-smooth-scroll"
                                href="http://demos.artbees.net/jupiter5/fortuna/rooms/">Rooms</a></li>
                        <li id="responsive-menu-item-100"
                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                class="menu-item-link js-smooth-scroll"
                                href="http://demos.artbees.net/jupiter5/fortuna/gallery/">Gallery</a></li>
                        <li id="responsive-menu-item-69"
                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                class="menu-item-link js-smooth-scroll"
                                href="http://demos.artbees.net/jupiter5/fortuna/reservation/">Reservation</a>
                        </li>
                        <li id="responsive-menu-item-97"
                            class="menu-item menu-item-type-post_type menu-item-object-page"><a
                                class="menu-item-link js-smooth-scroll"
                                href="http://demos.artbees.net/jupiter5/fortuna/contact-us/">Contact Us</a></li>
                    </ul>
                </nav>
                <form class="responsive-searchform" method="get"
                      action="http://demos.artbees.net/jupiter5/fortuna/">
                    <input type="text" class="text-input" value="" name="s" id="s" placeholder="Search..">
                    <i><input value="" type="submit">
                        <svg class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-590450552d6d0"
                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1664 1792">
                            <path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"></path>
                        </svg>
                    </i>
                </form>


            </div>

        </div>
    </div>

</header>