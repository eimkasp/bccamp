<!DOCTYPE html>
<!-- saved from url=(0042)http://demos.artbees.net/jupiter5/fortuna/ -->
<html lang="en-US"
      class="Chrome Chrome58 cssanimations csstransitions no-touchevents js_active  vc_desktop  vc_transform  vc_transform">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="format-detection" content="telephone=no">
  <title>BC Camp - Basketball camp in Lithuania</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&amp;subset=latin-ext" rel="stylesheet">
  <style id="critical-path-css" type="text/css">body, html {
      width: 100%;
      height: 100%;
      margin: 0;
      padding: 0
    }

    .page-preloader {
      top: 0;
      left: 0;
      z-index: 999;
      position: fixed;
      height: 100%;
      width: 100%;
      text-align: center
    }

    .preloader-logo, .preloader-preview-area {
      top: 50%;
      max-height: calc(50% - 20px);
      opacity: 1
    }

    .preloader-preview-area {
      -webkit-animation-delay: -.2s;
      animation-delay: -.2s;
      -webkit-transform: translateY(100%);
      -ms-transform: translateY(100%);
      transform: translateY(100%);
      margin-top: 10px;
      width: 100%;
      text-align: center;
      position: absolute
    }

    .preloader-logo {
      max-width: 90%;
      -webkit-transform: translateY(-100%);
      -ms-transform: translateY(-100%);
      transform: translateY(-100%);
      margin: -10px auto 0;
      position: relative
    }

    .ball-pulse > div, .ball-scale > div, .line-scale > div {
      margin: 2px;
      display: inline-block
    }

    .ball-pulse > div {
      width: 15px;
      height: 15px;
      border-radius: 100%;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      -webkit-animation: ball-pulse .75s infinite cubic-bezier(.2, .68, .18, 1.08);
      animation: ball-pulse .75s infinite cubic-bezier(.2, .68, .18, 1.08)
    }

    .ball-pulse > div:nth-child(1) {
      -webkit-animation-delay: -.36s;
      animation-delay: -.36s
    }

    .ball-pulse > div:nth-child(2) {
      -webkit-animation-delay: -.24s;
      animation-delay: -.24s
    }

    .ball-pulse > div:nth-child(3) {
      -webkit-animation-delay: -.12s;
      animation-delay: -.12s
    }

    @-webkit-keyframes ball-pulse {
      0%, 80% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 1
      }
      45% {
        -webkit-transform: scale(.1);
        transform: scale(.1);
        opacity: .7
      }
    }

    @keyframes ball-pulse {
      0%, 80% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 1
      }
      45% {
        -webkit-transform: scale(.1);
        transform: scale(.1);
        opacity: .7
      }
    }

    .ball-clip-rotate-pulse {
      position: relative;
      -webkit-transform: translateY(-15px) translateX(-10px);
      -ms-transform: translateY(-15px) translateX(-10px);
      transform: translateY(-15px) translateX(-10px);
      display: inline-block
    }

    .ball-clip-rotate-pulse > div {
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      position: absolute;
      top: 0;
      left: 0;
      border-radius: 100%
    }

    .ball-clip-rotate-pulse > div:first-child {
      height: 36px;
      width: 36px;
      top: 7px;
      left: -7px;
      -webkit-animation: ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
      animation: ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09, .57, .49, .9) infinite
    }

    .ball-clip-rotate-pulse > div:last-child {
      position: absolute;
      width: 50px;
      height: 50px;
      left: -16px;
      top: -2px;
      background: 0 0;
      border: 2px solid;
      -webkit-animation: ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
      animation: ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
      -webkit-animation-duration: 1s;
      animation-duration: 1s
    }

    @-webkit-keyframes ball-clip-rotate-pulse-rotate {
      0% {
        -webkit-transform: rotate(0) scale(1);
        transform: rotate(0) scale(1)
      }
      50% {
        -webkit-transform: rotate(180deg) scale(.6);
        transform: rotate(180deg) scale(.6)
      }
      100% {
        -webkit-transform: rotate(360deg) scale(1);
        transform: rotate(360deg) scale(1)
      }
    }

    @keyframes ball-clip-rotate-pulse-rotate {
      0% {
        -webkit-transform: rotate(0) scale(1);
        transform: rotate(0) scale(1)
      }
      50% {
        -webkit-transform: rotate(180deg) scale(.6);
        transform: rotate(180deg) scale(.6)
      }
      100% {
        -webkit-transform: rotate(360deg) scale(1);
        transform: rotate(360deg) scale(1)
      }
    }

    @-webkit-keyframes ball-clip-rotate-pulse-scale {
      30% {
        -webkit-transform: scale(.3);
        transform: scale(.3)
      }
      100% {
        -webkit-transform: scale(1);
        transform: scale(1)
      }
    }

    @keyframes ball-clip-rotate-pulse-scale {
      30% {
        -webkit-transform: scale(.3);
        transform: scale(.3)
      }
      100% {
        -webkit-transform: scale(1);
        transform: scale(1)
      }
    }

    @-webkit-keyframes square-spin {
      25% {
        -webkit-transform: perspective(100px) rotateX(180deg) rotateY(0);
        transform: perspective(100px) rotateX(180deg) rotateY(0)
      }
      50% {
        -webkit-transform: perspective(100px) rotateX(180deg) rotateY(180deg);
        transform: perspective(100px) rotateX(180deg) rotateY(180deg)
      }
      75% {
        -webkit-transform: perspective(100px) rotateX(0) rotateY(180deg);
        transform: perspective(100px) rotateX(0) rotateY(180deg)
      }
      100% {
        -webkit-transform: perspective(100px) rotateX(0) rotateY(0);
        transform: perspective(100px) rotateX(0) rotateY(0)
      }
    }

    @keyframes square-spin {
      25% {
        -webkit-transform: perspective(100px) rotateX(180deg) rotateY(0);
        transform: perspective(100px) rotateX(180deg) rotateY(0)
      }
      50% {
        -webkit-transform: perspective(100px) rotateX(180deg) rotateY(180deg);
        transform: perspective(100px) rotateX(180deg) rotateY(180deg)
      }
      75% {
        -webkit-transform: perspective(100px) rotateX(0) rotateY(180deg);
        transform: perspective(100px) rotateX(0) rotateY(180deg)
      }
      100% {
        -webkit-transform: perspective(100px) rotateX(0) rotateY(0);
        transform: perspective(100px) rotateX(0) rotateY(0)
      }
    }

    .square-spin {
      display: inline-block
    }

    .square-spin > div {
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      width: 50px;
      height: 50px;
      -webkit-animation: square-spin 3s 0s cubic-bezier(.09, .57, .49, .9) infinite;
      animation: square-spin 3s 0s cubic-bezier(.09, .57, .49, .9) infinite
    }

    .cube-transition {
      position: relative;
      -webkit-transform: translate(-25px, -25px);
      -ms-transform: translate(-25px, -25px);
      transform: translate(-25px, -25px);
      display: inline-block
    }

    .cube-transition > div {
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      width: 15px;
      height: 15px;
      position: absolute;
      top: -5px;
      left: -5px;
      -webkit-animation: cube-transition 1.6s 0s infinite ease-in-out;
      animation: cube-transition 1.6s 0s infinite ease-in-out
    }

    .cube-transition > div:last-child {
      -webkit-animation-delay: -.8s;
      animation-delay: -.8s
    }

    @-webkit-keyframes cube-transition {
      25% {
        -webkit-transform: translateX(50px) scale(.5) rotate(-90deg);
        transform: translateX(50px) scale(.5) rotate(-90deg)
      }
      50% {
        -webkit-transform: translate(50px, 50px) rotate(-180deg);
        transform: translate(50px, 50px) rotate(-180deg)
      }
      75% {
        -webkit-transform: translateY(50px) scale(.5) rotate(-270deg);
        transform: translateY(50px) scale(.5) rotate(-270deg)
      }
      100% {
        -webkit-transform: rotate(-360deg);
        transform: rotate(-360deg)
      }
    }

    @keyframes cube-transition {
      25% {
        -webkit-transform: translateX(50px) scale(.5) rotate(-90deg);
        transform: translateX(50px) scale(.5) rotate(-90deg)
      }
      50% {
        -webkit-transform: translate(50px, 50px) rotate(-180deg);
        transform: translate(50px, 50px) rotate(-180deg)
      }
      75% {
        -webkit-transform: translateY(50px) scale(.5) rotate(-270deg);
        transform: translateY(50px) scale(.5) rotate(-270deg)
      }
      100% {
        -webkit-transform: rotate(-360deg);
        transform: rotate(-360deg)
      }
    }

    .ball-scale > div {
      border-radius: 100%;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      height: 60px;
      width: 60px;
      -webkit-animation: ball-scale 1s 0s ease-in-out infinite;
      animation: ball-scale 1s 0s ease-in-out infinite
    }

    .ball-scale-multiple > div, .line-scale > div {
      -webkit-animation-fill-mode: both;
      height: 50px
    }

    @-webkit-keyframes ball-scale {
      0% {
        -webkit-transform: scale(0);
        transform: scale(0)
      }
      100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0
      }
    }

    @keyframes ball-scale {
      0% {
        -webkit-transform: scale(0);
        transform: scale(0)
      }
      100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0
      }
    }

    .line-scale > div {
      animation-fill-mode: both;
      width: 5px;
      border-radius: 2px
    }

    .line-scale > div:nth-child(1) {
      -webkit-animation: line-scale 1s -.5s infinite cubic-bezier(.2, .68, .18, 1.08);
      animation: line-scale 1s -.5s infinite cubic-bezier(.2, .68, .18, 1.08)
    }

    .line-scale > div:nth-child(2) {
      -webkit-animation: line-scale 1s -.4s infinite cubic-bezier(.2, .68, .18, 1.08);
      animation: line-scale 1s -.4s infinite cubic-bezier(.2, .68, .18, 1.08)
    }

    .line-scale > div:nth-child(3) {
      -webkit-animation: line-scale 1s -.3s infinite cubic-bezier(.2, .68, .18, 1.08);
      animation: line-scale 1s -.3s infinite cubic-bezier(.2, .68, .18, 1.08)
    }

    .line-scale > div:nth-child(4) {
      -webkit-animation: line-scale 1s -.2s infinite cubic-bezier(.2, .68, .18, 1.08);
      animation: line-scale 1s -.2s infinite cubic-bezier(.2, .68, .18, 1.08)
    }

    .line-scale > div:nth-child(5) {
      -webkit-animation: line-scale 1s -.1s infinite cubic-bezier(.2, .68, .18, 1.08);
      animation: line-scale 1s -.1s infinite cubic-bezier(.2, .68, .18, 1.08)
    }

    @-webkit-keyframes line-scale {
      0%, 100% {
        -webkit-transform: scaley(1);
        transform: scaley(1)
      }
      50% {
        -webkit-transform: scaley(.4);
        transform: scaley(.4)
      }
    }

    @keyframes line-scale {
      0%, 100% {
        -webkit-transform: scaley(1);
        transform: scaley(1)
      }
      50% {
        -webkit-transform: scaley(.4);
        transform: scaley(.4)
      }
    }

    .ball-scale-multiple {
      position: relative;
      -webkit-transform: translateY(30px);
      -ms-transform: translateY(30px);
      transform: translateY(30px);
      display: inline-block
    }

    .ball-scale-multiple > div {
      border-radius: 100%;
      animation-fill-mode: both;
      margin: 0;
      position: absolute;
      left: -30px;
      top: 0;
      opacity: 0;
      width: 50px;
      -webkit-animation: ball-scale-multiple 1s 0s linear infinite;
      animation: ball-scale-multiple 1s 0s linear infinite
    }

    .ball-scale-multiple > div:nth-child(2), .ball-scale-multiple > div:nth-child(3) {
      -webkit-animation-delay: -.2s;
      animation-delay: -.2s
    }

    @-webkit-keyframes ball-scale-multiple {
      0% {
        -webkit-transform: scale(0);
        transform: scale(0);
        opacity: 0
      }
      5% {
        opacity: 1
      }
      100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0
      }
    }

    @keyframes ball-scale-multiple {
      0% {
        -webkit-transform: scale(0);
        transform: scale(0);
        opacity: 0
      }
      5% {
        opacity: 1
      }
      100% {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 0
      }
    }

    .ball-pulse-sync {
      display: inline-block
    }

    .ball-pulse-sync > div {
      width: 15px;
      height: 15px;
      border-radius: 100%;
      margin: 2px;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      display: inline-block
    }

    .ball-pulse-sync > div:nth-child(1) {
      -webkit-animation: ball-pulse-sync .6s -.21s infinite ease-in-out;
      animation: ball-pulse-sync .6s -.21s infinite ease-in-out
    }

    .ball-pulse-sync > div:nth-child(2) {
      -webkit-animation: ball-pulse-sync .6s -.14s infinite ease-in-out;
      animation: ball-pulse-sync .6s -.14s infinite ease-in-out
    }

    .ball-pulse-sync > div:nth-child(3) {
      -webkit-animation: ball-pulse-sync .6s -70ms infinite ease-in-out;
      animation: ball-pulse-sync .6s -70ms infinite ease-in-out
    }

    @-webkit-keyframes ball-pulse-sync {
      33% {
        -webkit-transform: translateY(10px);
        transform: translateY(10px)
      }
      66% {
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px)
      }
      100% {
        -webkit-transform: translateY(0);
        transform: translateY(0)
      }
    }

    @keyframes ball-pulse-sync {
      33% {
        -webkit-transform: translateY(10px);
        transform: translateY(10px)
      }
      66% {
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px)
      }
      100% {
        -webkit-transform: translateY(0);
        transform: translateY(0)
      }
    }

    .transparent-circle {
      display: inline-block;
      border-top: .5em solid rgba(255, 255, 255, .2);
      border-right: .5em solid rgba(255, 255, 255, .2);
      border-bottom: .5em solid rgba(255, 255, 255, .2);
      border-left: .5em solid #fff;
      -webkit-transform: translateZ(0);
      transform: translateZ(0);
      -webkit-animation: transparent-circle 1.1s infinite linear;
      animation: transparent-circle 1.1s infinite linear;
      width: 50px;
      height: 50px;
      border-radius: 50%
    }

    .transparent-circle:after {
      border-radius: 50%;
      width: 10em;
      height: 10em
    }

    @-webkit-keyframes transparent-circle {
      0% {
        -webkit-transform: rotate(0);
        transform: rotate(0)
      }
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg)
      }
    }

    @keyframes transparent-circle {
      0% {
        -webkit-transform: rotate(0);
        transform: rotate(0)
      }
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg)
      }
    }

    .ball-spin-fade-loader {
      position: relative;
      top: -10px;
      left: -10px;
      display: inline-block
    }

    .ball-spin-fade-loader > div {
      width: 15px;
      height: 15px;
      border-radius: 100%;
      margin: 2px;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      position: absolute;
      -webkit-animation: ball-spin-fade-loader 1s infinite linear;
      animation: ball-spin-fade-loader 1s infinite linear
    }

    .ball-spin-fade-loader > div:nth-child(1) {
      top: 25px;
      left: 0;
      animation-delay: -.84s;
      -webkit-animation-delay: -.84s
    }

    .ball-spin-fade-loader > div:nth-child(2) {
      top: 17.05px;
      left: 17.05px;
      animation-delay: -.72s;
      -webkit-animation-delay: -.72s
    }

    .ball-spin-fade-loader > div:nth-child(3) {
      top: 0;
      left: 25px;
      animation-delay: -.6s;
      -webkit-animation-delay: -.6s
    }

    .ball-spin-fade-loader > div:nth-child(4) {
      top: -17.05px;
      left: 17.05px;
      animation-delay: -.48s;
      -webkit-animation-delay: -.48s
    }

    .ball-spin-fade-loader > div:nth-child(5) {
      top: -25px;
      left: 0;
      animation-delay: -.36s;
      -webkit-animation-delay: -.36s
    }

    .ball-spin-fade-loader > div:nth-child(6) {
      top: -17.05px;
      left: -17.05px;
      animation-delay: -.24s;
      -webkit-animation-delay: -.24s
    }

    .ball-spin-fade-loader > div:nth-child(7) {
      top: 0;
      left: -25px;
      animation-delay: -.12s;
      -webkit-animation-delay: -.12s
    }

    .ball-spin-fade-loader > div:nth-child(8) {
      top: 17.05px;
      left: -17.05px;
      animation-delay: 0s;
      -webkit-animation-delay: 0s
    }

    @-webkit-keyframes ball-spin-fade-loader {
      50% {
        opacity: .3;
        -webkit-transform: scale(.4);
        transform: scale(.4)
      }
      100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1)
      }
    }

    @keyframes ball-spin-fade-loader {
      50% {
        opacity: .3;
        -webkit-transform: scale(.4);
        transform: scale(.4)
      }
      100% {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1)
      }
    }</style>
  <link rel="dns-prefetch" href="http://fonts.googleapis.com/">
  <link rel="dns-prefetch" href="http://s.w.org/">


  <link rel="shortcut icon"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/2016/01/favicon.png">
  <link rel="stylesheet" id="contact-form-7-css" href="resources/styles.css" type="text/css" media="all">
  <link rel="stylesheet" id="theme-styles-css" href="resources/core-styles.css" type="text/css" media="all">
  <link rel="stylesheet" id="google-font-api-special-1-css" href="resources/css" type="text/css" media="all">
  <link rel="stylesheet" id="js_composer_front-css" href="resources/js_composer.min.css" type="text/css" media="all">
  <link rel="stylesheet" id="global-assets-css-css" href="resources/components-full.css" type="text/css" media="all">
  <link rel="stylesheet" id="theme-options-css" href="resources/theme-options-production.css" type="text/css"
        media="all">
  <link rel="stylesheet" id="mk-style-css" href="resources/style.css" type="text/css" media="all">
  <link rel="stylesheet" id="theme-dynamic-styles-css" href="resources/custom.css" type="text/css" media="all">
  <style id="theme-dynamic-styles-inline-css" type="text/css">
    body {
      background-color: #fff;
    }

    .mk-header {
      background-color: #f7f7f7;
      background-size: cover;
      -webkit-background-size: cover;
      -moz-background-size: cover;
    }

    .mk-header-bg {
      background-color: #1d161e;
    }

    .mk-classic-nav-bg {
      background-color: #1d161e;
    }

    .master-holder-bg {
      background-color: #fff;
    }

    #mk-footer {
      background-color: #3d4045;
    }

    #mk-boxed-layout {
      -webkit-box-shadow: 0 0 0px rgba(0, 0, 0, 0);
      -moz-box-shadow: 0 0 0px rgba(0, 0, 0, 0);
      box-shadow: 0 0 0px rgba(0, 0, 0, 0);
    }

    .mk-news-tab .mk-tabs-tabs .is-active a, .mk-fancy-title.pattern-style span, .mk-fancy-title.pattern-style.color-gradient span:after, .page-bg-color {
      background-color: #fff;
    }

    .page-title {
      font-size: 20px;
      color: #4d4d4d;
      text-transform: uppercase;
      font-weight: 400;
      letter-spacing: 2px;
    }

    .page-subtitle {
      font-size: 14px;
      line-height: 100%;
      color: #a3a3a3;
      font-size: 14px;
      text-transform: none;
    }

    .header-style-1 .mk-header-padding-wrapper, .header-style-2 .mk-header-padding-wrapper, .header-style-3 .mk-header-padding-wrapper {
      padding-top: 91px;
    }

    @font-face {
      font-family: 'star';
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot');
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot?#iefix') format('embedded-opentype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.woff') format('woff'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.ttf') format('truetype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.svg#star') format('svg');
      font-weight: normal;
      font-style: normal;
    }

    @font-face {
      font-family: 'WooCommerce';
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot');
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot?#iefix') format('embedded-opentype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.woff') format('woff'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.ttf') format('truetype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.svg#WooCommerce') format('svg');
      font-weight: normal;
      font-style: normal;
    }

    #mk-ornamental-title-3 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-3 .title {
      color: #ffffff;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-3 .title span::after, #mk-ornamental-title-3 .title span::before {
      border-top: 1px solid #ffffff;
    }

    #mk-ornamental-title-3 .title {
      font-size: 50px;
      line-height: 56px;
    }

    #mk-ornamental-title-3 .title span::after, #mk-ornamental-title-3 .title span::before {
      top: 27.5px;
    }

    #fancy-title-4 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 80px;
      color: #ffffff;
      text-align: center;
      font-style: inherit;
      font-weight: bold;
      padding-top: 0px;
    }

    #fancy-title-4 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-4 {
        text-align: center !important;
      }
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-4 {
        font-size: 30px;
      }
    }

    #text-block-5 {
      margin-bottom: 30px;
      text-align: center;
    }

    .mk-ornamental-title-6 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    .mk-ornamental-title-6 .title {
      color: #fff588;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    .mk-ornamental-title-6 .title {
      font-size: 20px;
      line-height: 26px;
    }

    .mk-ornamental-title-6 .title span {
      border: 1px solid #fff588;
    }

    .full-width-2 .mk-video-color-mask {
      background: #e3aa7f;
      background: -moz-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e3aa7f), color-stop(100%, #26001f));
      background: -webkit-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: -o-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: -ms-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: linear-gradient(to bottom, #e3aa7f 0%, #26001f 100%);
    }

    .full-width-2 {
      min-height: 100px;
      margin-bottom: 0px;
    }

    .full-width-2 .page-section-content {
      padding: 0px 0 0px;
    }

    #background-layer--2 {;
      background-position: center top;
      background-repeat: no-repeat;;
    }

    #background-layer--2 .mk-color-layer {;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
    }

    #padding-8 {
      height: 145px;
    }

    #mk-ornamental-title-10 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-10 .title {
      color: #222222;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-10 .title span::after, #mk-ornamental-title-10 .title span::before {
      border-top: 1px solid #222222;
    }

    #mk-ornamental-title-10 .title {
      font-size: 35px;
      line-height: 41px;
    }

    #mk-ornamental-title-10 .title span::after, #mk-ornamental-title-10 .title span::before {
      top: 20px;
    }

    #fancy-title-11 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 35px;
      color: #222222;
      text-align: center;
      font-style: inherit;
      font-weight: 400;
      padding-top: 0px;
      padding-bottom: px;
    }

    #fancy-title-11 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-11 {
        text-align: center !important;
      }
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-11 {
        font-size: 30px;
      }
    }

    #text-block-12 {
      margin-bottom: 30px;
      text-align: center;
    }

    #mk-ornamental-title-13 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-13 .title {
      color: #44c6d5;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-13 .title span::after, #mk-ornamental-title-13 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-13 .title {
      font-size: 14px;
      line-height: 20px;
    }

    #mk-ornamental-title-13 .title span::after {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-13 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #box-9 .box-holder {
      border: 5px solid #f5f1c4;
    }

    #box-9 .box-holder {
      background-color: rgba(255, 255, 255, 0.8);
    }

    #box-9 {
      margin-bottom: 0px;
    }

    #box-9 .box-holder {
      min-height: 100px;
      padding: 70px 60px;
    }

    #box-9 .box-holder:hover {
      background-color:;
    }

    #mk-shape-divider-14 .shape__container {
      background-color:;
    }

    #mk-shape-divider-14 .shape__container .shape {
      overflow: hidden;
      height:;
    }

    #mk-shape-divider-14 .shape__container .shape svg {
      position: relative;
      top: 0.6px;
    }

    #mk-shape-divider-14 .shape__container .shape .speech-left, #mk-shape-divider-14 .shape__container .shape .speech-right {
      background-color: #fbfbfb;
    }

    .full-width-7 {
      min-height: 780px;
      margin-bottom: 0px;
    }

    .full-width-7 .page-section-content {
      padding: 0px 0 100px;
    }

    #background-layer--7 {;
      background-position: center top;
      background-repeat: no-repeat;;
    }

    #background-layer--7 .mk-color-layer {;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
    }

    .full-width-7 .mk-skip-to-next {
      bottom: 100px;
    }

    #padding-15 {
      height: 100px;
    }

    #fancy-title-16 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 35px;
      color: #c95a66;
      text-align: center;
      font-style: inherit;
      font-weight: 400;
      padding-top: 0px;
      padding-bottom: 0px;
    }

    #fancy-title-16 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-16 {
        text-align: center !important;
      }
    }

    #mk-ornamental-title-17 {
      margin-top: 0px;
      margin-bottom: 0px;
    }

    #mk-ornamental-title-17 .title {
      color: #888888;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-17 .title span::after, #mk-ornamental-title-17 .title span::before {
      border-top: 1px solid #888888;
    }

    #mk-ornamental-title-17 .title {
      font-size: 25px;
      line-height: 31px;
    }

    #mk-ornamental-title-17 .title span::after, #mk-ornamental-title-17 .title span::before {
      top: 15px;
    }

    #padding-18 {
      height: 50px;
    }

    #padding-20 {
      height: 30px;
    }

    #fancy-title-21 {
      letter-spacing: 2px;
      text-transform: uppercase;
      font-size: 20px;
      color: #222222;
      text-align: center;
      font-style: inherit;
      font-weight: bold;
      padding-top: 0px;
      padding-bottom: 0px;
    }

    #fancy-title-21 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-21 {
        text-align: center !important;
      }
    }

    #text-block-22 {
      margin-bottom: 0px;
      text-align: center;
    }

    #mk-ornamental-title-23 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-23 .title {
      color: #44c6d5;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-23 .title span::after, #mk-ornamental-title-23 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-23 .title {
      font-size: 12px;
      line-height: 18px;
    }

    #mk-ornamental-title-23 .title span::after {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-23 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #box-19 .box-holder {
      background-color: #ffffff;
    }

    #box-19 {
      margin-bottom: 0px;
    }

    #box-19 .box-holder {
      min-height: 430px;
      padding: 80px 80px;
    }

    #box-19 .box-holder:hover {
      background-color:;
    }

    #box-24 .box-holder::after, #box-24.hover-effect-image.image-effect-blur .box-holder::before {
      content: "";
      background-image: url(http://stovykla.basketnews.lt/paveikslelis-138946-any586x600.jpg);
      background-position: center center;
      background-repeat: no-repeat;
    }

    #box-24 .box-holder::after, #box-24.hover-effect-image.image-effect-blur .box-holder::before {
      content: "";
      background-image: url(http://stovykla.basketnews.lt/paveikslelis-138946-any586x600.jpg);
      background-position: center center;
      background-repeat: no-repeat;
    }

    #box-24 .box-holder {
      background-color:;
    }

    #box-24 .box-holder::after, #box-24.hover-effect-image.image-effect-blur .box-holder::before {
      background-size: cover;
      -webkit-background-size: cover;
      -moz-background-size: cover;
    }

    #box-24 {
      margin-bottom: 0px;
    }

    #box-24 .box-holder {
      min-height: 430px;
      padding: 80px 80px;
    }

    #box-24 .box-holder:hover {
      background-color:;
    }

    #box-25 .box-holder::after, #box-25.hover-effect-image.image-effect-blur .box-holder::before {
      content: "";
      background-image: url(http://ambertonhotels.com/wp_amberton/wp-content/uploads/2015/02/kauno_panorama-2-copy.jpg);
      background-position: center center;
      background-repeat: no-repeat;
    }

    #box-25 .box-holder::after, #box-25.hover-effect-image.image-effect-blur .box-holder::before {
      content: "";
      background-image: url(http://ambertonhotels.com/wp_amberton/wp-content/uploads/2015/02/kauno_panorama-2-copy.jpg);
      background-position: center center;
      background-repeat: no-repeat;
    }

    #box-25 .box-holder {
      background-color:;
    }

    #box-25 .box-holder::after, #box-25.hover-effect-image.image-effect-blur .box-holder::before {
      background-size: cover;
      -webkit-background-size: cover;
      -moz-background-size: cover;
    }

    #box-25 {
      margin-bottom: 0px;
    }

    #box-25 .box-holder {
      min-height: 430px;
      padding: 80px 80px;
    }

    #box-25 .box-holder:hover {
      background-color:;
    }

    #padding-27 {
      height: 30px;
    }

    #fancy-title-28 {
      letter-spacing: 2px;
      text-transform: uppercase;
      font-size: 20px;
      color: #222222;
      text-align: center;
      font-style: inherit;
      font-weight: bold;
      padding-top: 0px;
      padding-bottom: 0px;
    }

    #fancy-title-28 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-28 {
        text-align: center !important;
      }
    }

    #text-block-29 {
      margin-bottom: 0px;
      text-align: center;
    }

    #mk-ornamental-title-30 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-30 .title {
      color: #44c6d5;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-30 .title span::after, #mk-ornamental-title-30 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-30 .title {
      font-size: 12px;
      line-height: 18px;
    }

    #mk-ornamental-title-30 .title span::after {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-30 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #box-26 .box-holder {
      background-color: #ffffff;
    }

    #box-26 {
      margin-bottom: 0px;
    }

    #box-26 .box-holder {
      min-height: 430px;
      padding: 80px 80px;
    }

    #box-26 .box-holder:hover {
      background-color:;
    }

    #padding-31 {
      height: 100px;
    }

    #padding-33 {
      height: 145px;
    }

    #mk-ornamental-title-35 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-35 .title {
      color: #222222;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-35 .title span::after, #mk-ornamental-title-35 .title span::before {
      border-top: 1px solid #222222;
    }

    #mk-ornamental-title-35 .title {
      font-size: 35px;
      line-height: 41px;
    }

    #mk-ornamental-title-35 .title span::after, #mk-ornamental-title-35 .title span::before {
      top: 20px;
    }

    #fancy-title-36 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 35px;
      color: #222222;
      text-align: center;
      font-style: inherit;
      font-weight: 400;
      padding-top: 0px;
      padding-bottom: px;
    }

    #fancy-title-36 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-36 {
        text-align: center !important;
      }
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-36 {
        font-size: 30px;
      }
    }

    #text-block-37 {
      margin-bottom: 30px;
      text-align: center;
    }

    #mk-ornamental-title-38 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-38 .title {
      color: #44c6d5;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-38 .title span::after, #mk-ornamental-title-38 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-38 .title {
      font-size: 14px;
      line-height: 20px;
    }

    #mk-ornamental-title-38 .title span::after {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-38 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #box-34 .box-holder {
      border: 5px solid #f5f1c4;
    }

    #box-34 .box-holder {
      background-color: rgba(255, 255, 255, 0.8);
    }

    #box-34 {
      margin-bottom: 0px;
    }

    #box-34 .box-holder {
      min-height: 100px;
      padding: 70px 60px;
    }

    #box-34 .box-holder:hover {
      background-color:;
    }

    #mk-shape-divider-39 .shape__container {
      background-color:;
    }

    #mk-shape-divider-39 .shape__container .shape {
      overflow: hidden;
      height:;
    }

    #mk-shape-divider-39 .shape__container .shape svg {
      position: relative;
      top: 0.6px;
    }

    #mk-shape-divider-39 .shape__container .shape .speech-left, #mk-shape-divider-39 .shape__container .shape .speech-right {
      background-color: #ffffff;
    }

    .full-width-32 {
      min-height: 780px;
      margin-bottom: 0px;
    }

    .full-width-32 .page-section-content {
      padding: 0px 0 100px;
    }

    #background-layer--32 {;
      background-position: center top;
      background-repeat: no-repeat;;
    }

    #background-layer--32 .mk-color-layer {;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
    }

    .full-width-32 .mk-skip-to-next {
      bottom: 100px;
    }

    #padding-40 {
      height: 100px;
    }

    #fancy-title-41 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 35px;
      color: #c95a66;
      text-align: center;
      font-style: inherit;
      font-weight: 400;
      padding-top: 0px;
      padding-bottom: 20px;
    }

    #fancy-title-41 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-41 {
        text-align: center !important;
      }
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-41 {
        font-size: 30px;
      }
    }

    #mk-ornamental-title-42 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-42 .title {
      color: #888888;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-42 .title span::after, #mk-ornamental-title-42 .title span::before {
      border-top: 1px solid #888888;
    }

    #mk-ornamental-title-42 .title {
      font-size: 25px;
      line-height: 31px;
    }

    #mk-ornamental-title-42 .title span::after, #mk-ornamental-title-42 .title span::before {
      top: 15px;
    }

    #testimonial_43 .mk-testimonial-quote, #testimonial_43 .mk-testimonial-quote p {
      color: #777777;
    }

    #testimonial_43 .mk-testimonial-quote {
      font-size: 18px;
      font-style: italic;
      font-weight: bold;
      letter-spacing: 0px;
      text-transform: initial;
    }

    #testimonial_43 .mk-testimonial-quote * {
      font-style: italic !important;
      font-weight: bold !important;
    }

    #testimonial_43 .mk-testimonial-author {
      color: #444444;
    }

    #testimonial_43 .mk-testimonial-company {
      color: #777777;
    }

    #padding-44 {
      height: 100px;
    }

    #padding-46 {
      height: 20px;
    }

    #fancy-title-47 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 40px;
      color: #222222;
      text-align: left;
      font-style: inherit;
      font-weight: bold;
      padding-top: 0px;
      padding-bottom: px;
    }

    #fancy-title-47 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-47 {
        text-align: center !important;
      }
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-47 {
        font-size: 30px;
      }
    }

    #padding-48 {
      height: 20px;
    }

    #mk-ornamental-title-49 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-49 .title {
      color: #44c6d5;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-49 .title span::after, #mk-ornamental-title-49 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-49 .title {
      font-size: 25px;
      line-height: 31px;
    }

    #mk-ornamental-title-49 .title span::after {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-49 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #box-45 .box-holder {
      border: 5px solid #f5f1c4;
    }

    #box-45 .box-holder {
      background-color:;
    }

    #box-45 {
      margin-bottom: 0px;
    }

    #box-45 .box-holder {
      min-height: 80px;
      padding: 40px 50px;
    }

    #box-45 .box-holder:hover {
      background-color:;
    }

    #padding-50 {
      height: 150px;
    }

    #wpcf7-f511-p143-o1 .vc_row . wpb_column:first-child {
      padding-right: 15px;
    }

    #wpcf7-f511-p143-o1 .vc_row . wpb_column:last-child {
      padding-left: 15px;
    }

    #wpcf7-f511-p143-o1 input[type=text] {
      border: 0;
      background-color: #aa8dd3;
      height: 40px;
      line-height: 40px;
      width: 100%;
      margin-bottom: 20px;
      font-size: 14px;
      color: #222222 !important;
    }

    #wpcf7-f511-p143-o1 label {
      font-size: 16px;
      margin-bottom: 10px;
      text-align: left;
    }

    #wpcf7-f511-p143-o1 input[type=file] {
      margin-bottom: 20px;
    }

    #wpcf7-f511-p143-o1 textarea {
      border: 0;
      background-color: #aa8dd3;
      height: 125px;
      width: 100%;
      margin-bottom: 10px;
      font-size: 14px;
      resize: none;
      Color: #222222 !important;
    }

    #wpcf7-f511-p143-o1 input[type=submit] {
      padding: 15px 40px;
      background-color: #00ffff;
      border: 0;
      color: #623e95;
      margin-top: 15px;
      font-size: 14px;
    }

    @media handheld, only screen and (max-width: 767px) {
      #wpcf7-f511-p143-o1 .vc_row . wpb_column:first-child {
        padding-right: 0 !important;
      }

      #wpcf7-f511-p143-o1 .vc_row . wpb_column:last-child {
        padding-left: 0 !important;
      }
    }
  </style>
  <script type="text/javascript" src="resources/jquery.js"></script>
  <script type="text/javascript" src="resources/jquery-migrate.min.js"></script>
  <link rel="https://api.w.org/" href="http://demos.artbees.net/jupiter5/fortuna/wp-json/">
  <link rel="EditURI" type="application/rsd+xml" title="RSD"
        href="http://demos.artbees.net/jupiter5/fortuna/xmlrpc.php?rsd">
  <link rel="wlwmanifest" type="application/wlwmanifest+xml"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-includes/wlwmanifest.xml">
  <meta name="generator" content="WordPress 4.7.4">
  <link rel="canonical" href="http://demos.artbees.net/jupiter5/fortuna/">
  <link rel="shortlink" href="http://demos.artbees.net/jupiter5/fortuna/">
  <link rel="alternate" type="application/json+oembed"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemos.artbees.net%2Fjupiter5%2Ffortuna%2F">
  <link rel="alternate" type="text/xml+oembed"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemos.artbees.net%2Fjupiter5%2Ffortuna%2F&amp;format=xml">
  <script> var isTest = false; </script>
  <style id="js-media-query-css">.mk-event-countdown-ul:media( max-width: 750px ) li {
      width: 90%;
      display: block;
      margin: 0 auto 15px
    }

    .mk-process-steps:media( max-width: 960px ) ul:before {
      display: none !important
    }

    .mk-process-steps:media( max-width: 960px ) li {
      margin-bottom: 30px !important;
      width: 100% !important;
      text-align: center
    }

    .mk-event-countdown-ul.query_max-width_750px li {
      width: 90%;
      display: block;
      margin: 0 auto 15px
    }

    .mk-process-steps.query_max-width_960px ul:before {
      display: none !important
    }

    .mk-process-steps.query_max-width_960px li {
      margin-bottom: 30px !important;
      width: 100% !important;
      text-align: center
    }</style>
  <meta itemprop="author" content="">
  <meta itemprop="datePublished" content="January 14, 2016">
  <meta itemprop="dateModified" content="February 1, 2016">
  <meta itemprop="publisher" content="Fortuna Template - Jupiter WordPress Theme">
  <style type="text/css">.recentcomments a {
      display: inline !important;
      padding: 0 !important;
      margin: 0 !important;
    }</style>
  <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/css/vc_lte_ie9.min.css"
        media="screen"><![endif]-->
  <meta name="generator" content="Jupiter 5.9.1">
  <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1452764991530 {
      background-color: #fbfbfb !important;
    }</style>
  <noscript>&lt;style type="text/css"&gt; .wpb_animate_when_almost_visible { opacity: 1; }&lt;/style&gt;</noscript>
  <style type="text/css">.fancybox-margin {
      margin-right: 0px;
    }</style>
  <style type="text/css">@keyframes resizeanim {
                           from {
                             opacity: 0;
                           }
                           to {
                             opacity: 0;
                           }
                         }

    .resize-triggers {
      animation: 1ms resizeanim;
      visibility: hidden;
      opacity: 0;
    }

    .resize-triggers, .resize-triggers > div, .contract-trigger:before {
      content: " ";
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      height: 100%;
      width: 100%;
      overflow: hidden;
    }

    .resize-triggers > div {
      background: #eee;
      overflow: auto;
    }

    .contract-trigger:before {
      width: 200%;
      height: 200%;
    }</style>

  <link rel="stylesheet" href="main.css">
</head>

<body class="home page-template-default page page-id-5 vertical-header-enabled vertical-header-left logo-align-center wpb-js-composer js-comp-ver-5.1.1 vc_responsive"
      itemscope="itemscope" itemtype="https://schema.org/WebPage" data-adminbar="">


<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
<div id="top-of-page"></div>

<div id="mk-boxed-layout">

  <div id="mk-theme-container">
      <?php include("header.php"); ?>
    <div id="theme-page" class="master-holder  clearfix" itemscope="itemscope" itemtype="https://schema.org/Blog">
      <div class="master-holder-bg-holder">
        <div id="theme-page-bg" class="master-holder-bg js-el"></div>
      </div>
      <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-5" class="theme-page-wrapper mk-main-wrapper mk-grid full-layout no-padding ">
          <div class="theme-content no-padding" itemprop="mainEntityOfPage">
            <div class="clearboth"></div>
          </div>
        </div>
      </div>


      <div class="mk-page-section-wrapper">
        <div id="page-section-2"
             class="mk-page-section self-hosted   full_layout full-width-2 js-el js-master-row     center-y mk-in-viewport"
             data-mk-component="FullHeight" data-fullheight-config="{&quot;min&quot;: 100}"
             data-intro-effect="false" style="min-height: 100vh;">
          <div class="mk-page-section-inner">
            <div style="background-color:#e3aa7f;opacity:0.8;" class="mk-video-color-mask"></div>
            <div class="background-layer-holder">
              <div id="background-layer--2"
                   class="background-layer mk-background-stretch none-blend-effect js-el"
                   style="background-image: url(bg.jpg); ">
                <div class="mk-color-layer"></div>
              </div>
            </div>
          </div>


          <div class="page-section-content vc_row-fluid mk-grid">
            <div class="mk-padding-wrapper">
              <div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
                <style>#mk-ornamental-title-3 {
                    font-family: "Parisienne"
                  }</style>
                <div id="mk-ornamental-title-3"
                     class="mk-ornamental-title norman-short-single align-center title_as_text">
                  <h2 class="title">
                    <span class="title-text">
                      Basketball camp
                    </span>
                  </h2>
                </div>
                <h2 id="fancy-title-4" class="mk-fancy-title  simple-style  color-single">
                  <span>BC Camp</span>
                </h2>
                <div class="clearboth"></div>
                <div id="text-block-5" class="mk-text-block">
                  <p>
                    <span style="color: #ffffff;">In KAUNAS, Lithuania</span>
                  </p>

                  <div class="clearboth"></div>
                </div>
                <div class="buttons-holder">
                  <div class="mk-ornamental-title lemo-single align-center title_as_text mk-ornamental-title-6">
                    <h2 class="title">
                      <span class="title-text">
                        <a href="http://kasperiunas.com/bccamp.lt/vidinis.php">
                          U14-U18 Camp <br>
                          <div class="small-size additional-info">Boys/Girls</div>
                        </a>
                      </span>
                    </h2>
                  </div>

                  <div class="mk-ornamental-title lemo-single align-center title_as_text mk-ornamental-title-6">
                    <h2 class="title">
                      <span class="title-text">
                        <a href="http://kasperiunas.com/bccamp.lt/vidinis.php">
                          U8-U13 Camp<br>
                          <div class="small-size additional-info">Boys/Girls</div>
                        </a>
                      </span>
                    </h2>
                  </div>
                </div>

              </div>
            </div>
            <div class="clearboth"></div>
          </div>


          <div class="mk-skip-to-next" data-skin="light">
            <svg class="mk-svg-icon" data-name="mk-jupiter-icon-arrow-bottom"
                 data-cacheid="icon-5904505530e97" style=" height:16px; width: 16px; "
                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
              <path d="M512 121.6c0 8-3.2 16-8 22.4l-225.6 240c-6.4 6.4-14.4 9.6-24 9.6-8 0-16-3.2-22.4-9.6l-224-240c-11.2-12.8-11.2-33.6 1.6-44.8 12.8-12.8 32-11.2 44.8 1.6l201.6 214.4 203.2-216c11.2-12.8 32-12.8 44.8 0 4.8 6.4 8 14.4 8 22.4z"></path>
            </svg>
          </div>


          <div class="clearboth"></div>
        </div>
      </div>

      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
          <div class="theme-content no-padding">


            <div class="clearboth"></div>
          </div>
        </div>
      </div>


      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
          <div class="theme-content no-padding">


          </div>
        </div>
      </div>

      <div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false    vc_custom_1452764991530  js-master-row "
      >
        <div class="mk-grid">

          <div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">

            <div id="padding-15" class="mk-padding-divider   clearfix"></div>


            <h2 id="fancy-title-16" class="mk-fancy-title  simple-style  color-single">
	<span>
				Coaches and personel</span>
            </h2>
            <div class="clearboth"></div>


            <style>#mk-ornamental-title-17 {
                font-family: "Parisienne"
              }</style>


            <div id="padding-18" class="mk-padding-divider   clearfix"></div>


            <div class="wpb_row vc_inner vc_row vc_row-fluid   add-padding-0 attched-true   ">

              <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="mk-custom-box hover-effect-image" id="box-19">
                      <div class="box-holder">
                        <div id="padding-20" class="mk-padding-divider   clearfix"></div>


                        <h2 id="fancy-title-21"
                            class="mk-fancy-title  simple-style  color-single">
	<span>
				Coaches and personel	</span>
                        </h2>
                        <div class="clearboth"></div>


                        <div id="text-block-22" class="mk-text-block   ">


                          <p>Mauris blandit aliquet elit eget tincidunt nibh pulvinar nulla
                            quis lorem ut libero malesuada feugiat&nbsp;vestibulum acsit
                            diamen amet quam vehicula elementum sed sit amet dui.</p>

                          <div class="clearboth"></div>
                        </div>
                        <div id="mk-ornamental-title-23"
                             class="mk-ornamental-title lemo-single align-center title_as_text">
                          <h2 class="title"><span class="title-text">All coaches</span>
                          </h2>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="mk-custom-box hover-effect-image" id="box-24">
                      <div class="box-holder">
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="wpb_row vc_inner vc_row vc_row-fluid   add-padding-0 attched-true   ">

              <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="mk-custom-box hover-effect-image" id="box-25">
                      <div class="box-holder">
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner ">
                  <div class="wpb_wrapper">
                    <div class="mk-custom-box hover-effect-image" id="box-26">
                      <div class="box-holder">
                        <div id="padding-27" class="mk-padding-divider   clearfix"></div>


                        <h2 id="fancy-title-28"
                            class="mk-fancy-title  simple-style  color-single">
	<span>
				About Kaunas</span>
                        </h2>
                        <div class="clearboth"></div>


                        <div id="text-block-29" class="mk-text-block   ">


                          <p>Kaunas has long been renowned as the sportiest city in Lithuania and from Lithuanian
                            Independence Day remains at the leading place. Our city has developed a good infrastructure
                            to develop sports facilities for various sports at the same time basketball.

                          </p>

                          <div class="clearboth"></div>
                        </div>
                        <div id="mk-ornamental-title-30"
                             class="mk-ornamental-title lemo-single align-center title_as_text">
                          <h2 class="title"><span
                                    class="title-text">Read more</span>
                          </h2>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>


      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper full-layout mk-grid vc_row-fluid no-padding">
          <div class="theme-content no-padding">


            <div class="clearboth"></div>
          </div>
        </div>
      </div>


      <div class="mk-page-section-wrapper">
        <div id="page-section-32"
             class="mk-page-section self-hosted   full_layout full-width-32 js-el js-master-row    "
             data-intro-effect="false">


          <div class="mk-page-section-inner">


            <div style="background-color:#ffffff;opacity:0.6;" class="mk-video-color-mask"></div>


            <div class="background-layer-holder">
              <div id="background-layer--32"
                   class="background-layer mk-background-stretch none-blend-effect js-el"
                   data-mk-img-set="{&quot;landscape&quot;:{&quot;external&quot;:&quot;http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/2016/01/massage-theraphy-bg.jpg&quot;},&quot;responsive&quot;:&quot;true&quot;}"
                   style="background-image: url(https://s1.15min.lt/static/cache/ODgweDU4MCw0NTd4MjQ2LDYyMjkzMSxvcmlnaW5hbCwsaWQ9MTAwODIwMiw0MDY3MzE0MzI3/kauno-plaukimo-centras-52eb60742817a.jpg);">
                <div class="mk-color-layer"></div>
              </div>
            </div>

          </div>


          <div class="page-section-content vc_row-fluid mk-grid">
            <div class="mk-padding-wrapper">
              <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full">
              </div>

              <div style="" class="vc_col-sm-8 wpb_column column_container  _ height-full">

                <div id="padding-33" class="mk-padding-divider   clearfix"></div>


                <div class="mk-custom-box hover-effect-image" id="box-34">
                  <div class="box-holder">
                    <style>#mk-ornamental-title-35 {
                        font-family: "Parisienne"
                      }</style>
                    <div id="mk-ornamental-title-35"
                         class="mk-ornamental-title norman-short-single align-center title_as_text">
                      <h2 class="title"><span class="title-text">Activities and leisure time</span></h2>
                    </div>
                    <h2 id="fancy-title-36" class="mk-fancy-title  simple-style  color-single">
	<span>
						</span>
                    </h2>
                    <div class="clearboth"></div>


                    <div id="text-block-37" class="mk-text-block   ">


                      <p>Lorem ipsum dolor sit amet conse ctetur adipiscing elit&nbsp;aenean est
                        lacus tincidunt id vehicula et blandit atin sapien&nbsp;aliquam nec
                        interdum nibh…</p>

                      <div class="clearboth"></div>
                    </div>
                    <div id="mk-ornamental-title-38"
                         class="mk-ornamental-title lemo-single align-center title_as_text"><h2
                              class="title"><span class="title-text">read more</span></h2>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>

              <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full">
              </div>
            </div>
            <div class="clearboth"></div>
          </div>


          <div class="mk-shape-divider mk-shape-divider--stick speech-bottom-style big-size mk-shape-divider--stick-bottom"
               id="mk-shape-divider-39">

          </div>

          <div class="clearboth"></div>
        </div>
      </div>

      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
          <div class="theme-content no-padding">


          </div>
        </div>
      </div>


      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper full-layout mk-grid vc_row-fluid no-padding">
          <div class="theme-content no-padding">


            <div class="clearboth"></div>

            <div class="clearboth"></div>
          </div>
          <div class="clearboth"></div>

        </div>
      </div>


    </div>

    <section id="mk-footer-unfold-spacer"></section>

    <section id="mk-footer" class=" mk-footer-disable" role="contentinfo" itemscope="itemscope"
             itemtype="https://schema.org/WPFooter">
    </section>
    <div class="resize-triggers">
      <div class="expand-trigger">
        <div style="width: 1441px; height: 4570px;"></div>
      </div>
      <div class="contract-trigger"></div>
    </div>
  </div>
</div>

<div class="mk-fullscreen-search-overlay">
  <a href="http://demos.artbees.net/jupiter5/fortuna/#" class="mk-fullscreen-close">
    <svg class="mk-svg-icon" data-name="mk-moon-close-2" data-cacheid="icon-5904505543a84"
         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
      <path d="M390.628 345.372l-45.256 45.256-89.372-89.373-89.373 89.372-45.255-45.255 89.373-89.372-89.372-89.373 45.254-45.254 89.373 89.372 89.372-89.373 45.256 45.255-89.373 89.373 89.373 89.372z"></path>
    </svg>
  </a>
  <div class="mk-fullscreen-search-wrapper">
    <p>Start typing and press Enter to search</p>
    <form method="get" id="mk-fullscreen-searchform" action="http://demos.artbees.net/jupiter5/fortuna/">
      <input type="text" value="" name="s" id="mk-fullscreen-search-input">
      <i class="fullscreen-search-icon">
        <svg class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-5904505543d04"
             style=" height:25px; width: 23.214285714286px; " xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 1664 1792">
          <path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"></path>
        </svg>
      </i>
    </form>
  </div>
</div>


<footer id="mk_page_footer">
  <script type="text/javascript" src="resources/jquery.form.min.js"></script>
  <script type="text/javascript">
    /* <![CDATA[ */
    var _wpcf7 = {"recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"};
    /* ]]> */
  </script>
  <script type="text/javascript" src="resources/scripts.js"></script>
  <script type="text/javascript" src="resources/core-scripts.js"></script>
  <script type="text/javascript" src="resources/wp-embed.min.js"></script>
  <script type="text/javascript" src="resources/js_composer_front.min.js"></script>
  <script type="text/javascript" src="resources/components-full.js"></script>

</footer>

<div style="clear: both;"></div>
</body>
</html>