<?php include("main.php"); ?>

<body class="page-template-default page page-id-67 vertical-header-enabled vertical-header-left logo-align-center loading wpb-js-composer js-comp-ver-5.1.1 vc_responsive"
      itemscope="itemscope" itemtype="https://schema.org/WebPage" data-adminbar="">


<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
<div id="top-of-page"></div>

<div id="mk-boxed-layout">

    <div id="mk-theme-container">

        <?php include("header.php"); ?>

        <div id="theme-page" class="master-holder  clearfix" itemscope="itemscope" itemtype="https://schema.org/Blog">

            <div class="master-holder-bg-holder">
                <div id="theme-page-bg" class="master-holder-bg js-el"></div>
            </div>

            <div class="mk-main-wrapper-holder">

                <div id="mk-page-id-67" class="theme-page-wrapper mk-main-wrapper mk-grid full-layout no-padding ">
                    <div class="theme-content no-padding" itemprop="mainEntityOfPage">


                        <div class="clearboth"></div>
                    </div>
                </div>
            </div>


            <div class="mk-page-section-wrapper">
                <div id="page-section-2"
                     class="mk-page-section self-hosted   full_layout full-width-2 js-el js-master-row     center-y"
                     data-intro-effect="false">


                    <div class="mk-page-section-inner">


                        <div style="background-color:#e3aa7f;opacity:0.8;" class="mk-video-color-mask"></div>


                        <div class="background-layer-holder">
                            <div id="background-layer--2" class="background-layer mk-background-stretch none-blend-effect js-el"
                                 data-mk-img-set='{"landscape":{"external":"http://middletownbasketball.com/middletown%20site/camp2010.jpg"},"responsive":"true"}'>
                                <div class="mk-color-layer"></div>
                            </div>
                        </div>

                    </div>


                    <div class="page-section-content vc_row-fluid mk-grid">
                        <div class="mk-padding-wrapper">
                            <div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
                                <div id="mk-ornamental-title-3"
                                     class="mk-ornamental-title norman-short-single align-center title_as_text"><h2 class="title"><span
                                            class="title-text">About us</span></h2>
                                </div>
                            </div>
                        </div>
                        <div class="clearboth"></div>
                    </div>


                    <div class="mk-shape-divider mk-shape-divider--stick speech-bottom-style big-size mk-shape-divider--stick-bottom"
                         id="mk-shape-divider-4">
                        <div class="shape__container">
                            <div class="shape">

                                <div class="speech-left"></div>
                                <div class="speech-right"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="clearboth"></div>
                </div>
            </div>

            <div class="mk-main-wrapper-holder">
                <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
                    <div class="theme-content no-padding">


                    </div>
                </div>
            </div>

            <div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false     js-master-row ">
                <div class="mk-grid">


                    <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full">
                    </div>

                    <div style="" class="vc_col-sm-8 wpb_column column_container  _ height-full">

                        <div id="padding-5" class="mk-padding-divider   clearfix"></div>


                        <h2 id="fancy-title-6" class="mk-fancy-title  simple-style  color-single">
	<span>
				About BC CAMP	</span>
                        </h2>
                        <div class="clearboth"></div>


                        <style>#mk-ornamental-title-7 {
                                font-family: "Parisienne"
                            }</style>
                        <div id="mk-ornamental-title-7" class="mk-ornamental-title norman-short-single align-center title_as_text">
                            <h2 class="title"><span class="title-text">&amp;</span></h2>
                        </div>
                        <div id="text-block-8" class="mk-text-block   ">


                            <p>Quisque velit nisi pretium ut lacinia in elementum id enim sed porttitor lectus nibh donec rutrum
                                congue leo eget malesuada orbi maximus nibh nec nisl rutrum eget maximus.</p>

                            <div class="clearboth"></div>
                        </div>

                        <div id="padding-9" class="mk-padding-divider   clearfix"></div>

                        <div id="padding-10" class="mk-padding-divider   clearfix"></div>

                    </div>

                    <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full">
                    </div>

                </div>
            </div>


            <div class="mk-main-wrapper-holder">
                <div class="theme-page-wrapper full-layout mk-grid vc_row-fluid no-padding">
                    <div class="theme-content no-padding">


                        <div class="clearboth"></div>
                    </div>
                </div>
            </div>



            <div class="mk-main-wrapper-holder">
                <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
                    <div class="theme-content no-padding">


                        <div class="clearboth"></div>

                        <div class="clearboth"></div>
                    </div>
                    <div class="clearboth"></div>

                </div>
            </div>


        </div>

        <section id="mk-footer-unfold-spacer"></section>

        <section id="mk-footer" class=" mk-footer-disable" role="contentinfo" itemscope="itemscope"
                 itemtype="https://schema.org/WPFooter">
        </section>
    </div>
</div>

<div class="bottom-corner-btns js-bottom-corner-btns">
    <div class="mk-quick-contact-wrapper  js-bottom-corner-btn js-bottom-corner-btn--contact">

        <a href="#" class="mk-quick-contact-link">
            <svg class="mk-svg-icon" data-name="mk-icon-envelope" data-cacheid="icon-59075ca6f27c6"
                 style=" height:20px; width: 20px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
                <path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"/>
            </svg>
        </a>
        <div id="mk-quick-contact">
            <div class="mk-quick-contact-title">Contact Us</div>
            <p>We're not around right now. But you can send us an email and we'll get back to you, asap.</p>
            <form class="mk-contact-form" method="post" novalidate="novalidate">
                <input type="text" placeholder="Name*" required="required" id="name" name="name" class="text-input" value=""
                       tabindex="166"/>
                <input type="email" data-type="email" required="required" placeholder="Email*" id="email" name="email"
                       class="text-input" value="" tabindex="167"/>
                <textarea placeholder="Message*" required="required" id="content" name="content" class="textarea"
                          tabindex="168"></textarea>

                <div class="btn-cont">
                    <button tabindex="169" class="mk-progress-button mk-contact-button accent-bg-color button"
                            data-style="move-up">
                        <span class="mk-progress-button-content">Siųsti</span>
                        <span class="mk-progress">
                            <span class="mk-progress-inner"></span>
                        </span>
                        <span class="state-success"><svg class="mk-svg-icon" data-name="mk-moon-checkmark"
                                                         data-cacheid="icon-59075ca6f2c2c" xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 0 512 512"><path
                                    d="M432 64l-240 240-112-112-80 80 192 192 320-320z"/></svg></span>
                        <span class="state-error"><svg class="mk-svg-icon" data-name="mk-moon-close"
                                                       data-cacheid="icon-59075ca6f2f18" xmlns="http://www.w3.org/2000/svg"
                                                       viewBox="0 0 512 512"><path
                                    d="M507.331 411.33l-.006-.005-155.322-155.325 155.322-155.325.006-.005c1.672-1.673 2.881-3.627 3.656-5.708 2.123-5.688.912-12.341-3.662-16.915l-73.373-73.373c-4.574-4.573-11.225-5.783-16.914-3.66-2.08.775-4.035 1.984-5.709 3.655l-.004.005-155.324 155.326-155.324-155.325-.005-.005c-1.673-1.671-3.627-2.88-5.707-3.655-5.69-2.124-12.341-.913-16.915 3.66l-73.374 73.374c-4.574 4.574-5.784 11.226-3.661 16.914.776 2.08 1.985 4.036 3.656 5.708l.005.005 155.325 155.324-155.325 155.326-.004.005c-1.671 1.673-2.88 3.627-3.657 5.707-2.124 5.688-.913 12.341 3.661 16.915l73.374 73.373c4.575 4.574 11.226 5.784 16.915 3.661 2.08-.776 4.035-1.985 5.708-3.656l.005-.005 155.324-155.325 155.324 155.325.006.004c1.674 1.672 3.627 2.881 5.707 3.657 5.689 2.123 12.342.913 16.914-3.661l73.373-73.374c4.574-4.574 5.785-11.227 3.662-16.915-.776-2.08-1.985-4.034-3.657-5.707z"/></svg></span>
                    </button>
                </div>
                <input type="hidden" id="security" name="security" value="f477bf6287"/><input type="hidden"
                                                                                              name="_wp_http_referer"
                                                                                              value="/jupiter5/fortuna/reservation/"/>
                <input type="hidden" id="sh_id" name="sh_id" value="15"><input type="hidden" id="p_id" name="p_id" value="2342">
                <div class="contact-form-message clearfix"></div>
            </form>
            <div class="bottom-arrow"></div>
        </div>
    </div>
</div>


<div class="mk-fullscreen-search-overlay">
    <a href="#" class="mk-fullscreen-close">
        <svg class="mk-svg-icon" data-name="mk-moon-close-2" data-cacheid="icon-59075ca6f3319"
             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
            <path d="M390.628 345.372l-45.256 45.256-89.372-89.373-89.373 89.372-45.255-45.255 89.373-89.372-89.372-89.373 45.254-45.254 89.373 89.372 89.372-89.373 45.256 45.255-89.373 89.373 89.373 89.372z"/>
        </svg>
    </a>
    <div class="mk-fullscreen-search-wrapper">
        <p>Start typing and press Enter to search</p>
        <form method="get" id="mk-fullscreen-searchform" action="http://demos.artbees.net/jupiter5/fortuna/">
            <input type="text" value="" name="s" id="mk-fullscreen-search-input"/>
            <i class="fullscreen-search-icon">
                <svg class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-59075ca6f355e"
                     style=" height:25px; width: 23.214285714286px; " xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 1664 1792">
                    <path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/>
                </svg>
            </i>
        </form>
    </div>
</div>


<footer id="mk_page_footer">
    <script type="text/javascript">
      php = {
        hasAdminbar: false,
        json: (null != null) ? null : "",
        jsPath: 'http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js'
      };
    </script>
    <link rel='stylesheet' id='Parisienne-css'
          href='//fonts.googleapis.com/css?family=Parisienne%3A100italic%2C200italic%2C300italic%2C400italic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic%2C100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.7.4'
          type='text/css' media='all'/>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpcf7 = {"recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"};
        /* ]]> */
    </script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7'></script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js/plugins/wp-enqueue/smoothscroll.js?ver=5.9.1'></script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/comment-reply.min.js?ver=4.7.4'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var ajax_login_object = {
          "ajaxurl": "http:\/\/demos.artbees.net\/jupiter5\/fortuna\/wp-admin\/admin-ajax.php",
          "redirecturl": "http:\/\/demos.artbees.net\/jupiter5\/fortuna\/reservation\/",
          "loadingmessage": "Sending user info, please wait..."
        };
        /* ]]> */
    </script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js/core-scripts.js?ver=5.9.1'></script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/wp-embed.min.js?ver=4.7.4'></script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/js/dist/js_composer_front.min.js?ver=5.1.1'></script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js/min/components-full.js?ver=5.9.1'></script>
    <script type="text/javascript">
    </script>
    <script type="text/javascript">    window.get = {};
      window.get.captcha = function (enteredCaptcha) {
        return jQuery.get(ajaxurl, {action: "mk_validate_captcha_input", captcha: enteredCaptcha});
      };</script>
    <script>
      // Run this very early after DOM is ready
      (function ($) {
        // Prevent browser native behaviour of jumping to anchor
        // while preserving support for current links (shared across net or internally on page)
        var loc = window.location,
            hash = loc.hash;

        // Detect hashlink and change it's name with !loading appendix
        if (hash.length && hash.substring(1).length) {
          var $topLevelSections = $('.vc_row, .mk-main-wrapper-holder, .mk-page-section, #comments');
          var $section = $topLevelSections.filter('#' + hash.substring(1));
          // We smooth scroll only to page section and rows where we define our anchors.
          // This should prevent conflict with third party plugins relying on hash
          if (!$section.length)  return;
          // Mutate hash for some good reason - crazy jumps of browser. We want really smooth scroll on load
          // Discard loading state if it already exists in url (multiple refresh)
          hash = hash.replace('!loading', '');
          var newUrl = hash + '!loading';
          loc.hash = newUrl;
        }
      }(jQuery));
    </script>


</footer>
</body>
</html>