<!DOCTYPE html>
<html lang="en-US">
<head>

    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="format-detection" content="telephone=no">
    <title>Reservation &#8211; Fortuna Template &#8211; Jupiter WordPress Theme</title>
    <script type="text/javascript">var ajaxurl = "http://demos.artbees.net/jupiter5/fortuna/wp-admin/admin-ajax.php"</script>
    <style id="critical-path-css" type='text/css'>body, html {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0
        }

        .page-preloader {
            top: 0;
            left: 0;
            z-index: 999;
            position: fixed;
            height: 100%;
            width: 100%;
            text-align: center
        }

        .preloader-logo, .preloader-preview-area {
            top: 50%;
            max-height: calc(50% - 20px);
            opacity: 1
        }

        .preloader-preview-area {
            -webkit-animation-delay: -.2s;
            animation-delay: -.2s;
            -webkit-transform: translateY(100%);
            -ms-transform: translateY(100%);
            transform: translateY(100%);
            margin-top: 10px;
            width: 100%;
            text-align: center;
            position: absolute
        }

        .preloader-logo {
            max-width: 90%;
            -webkit-transform: translateY(-100%);
            -ms-transform: translateY(-100%);
            transform: translateY(-100%);
            margin: -10px auto 0;
            position: relative
        }

        .ball-pulse > div, .ball-scale > div, .line-scale > div {
            margin: 2px;
            display: inline-block
        }

        .ball-pulse > div {
            width: 15px;
            height: 15px;
            border-radius: 100%;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            -webkit-animation: ball-pulse .75s infinite cubic-bezier(.2, .68, .18, 1.08);
            animation: ball-pulse .75s infinite cubic-bezier(.2, .68, .18, 1.08)
        }

        .ball-pulse > div:nth-child(1) {
            -webkit-animation-delay: -.36s;
            animation-delay: -.36s
        }

        .ball-pulse > div:nth-child(2) {
            -webkit-animation-delay: -.24s;
            animation-delay: -.24s
        }

        .ball-pulse > div:nth-child(3) {
            -webkit-animation-delay: -.12s;
            animation-delay: -.12s
        }

        @-webkit-keyframes ball-pulse {
            0%, 80% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1
            }
            45% {
                -webkit-transform: scale(.1);
                transform: scale(.1);
                opacity: .7
            }
        }

        @keyframes ball-pulse {
            0%, 80% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 1
            }
            45% {
                -webkit-transform: scale(.1);
                transform: scale(.1);
                opacity: .7
            }
        }

        .ball-clip-rotate-pulse {
            position: relative;
            -webkit-transform: translateY(-15px) translateX(-10px);
            -ms-transform: translateY(-15px) translateX(-10px);
            transform: translateY(-15px) translateX(-10px);
            display: inline-block
        }

        .ball-clip-rotate-pulse > div {
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            position: absolute;
            top: 0;
            left: 0;
            border-radius: 100%
        }

        .ball-clip-rotate-pulse > div:first-child {
            height: 36px;
            width: 36px;
            top: 7px;
            left: -7px;
            -webkit-animation: ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
            animation: ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09, .57, .49, .9) infinite
        }

        .ball-clip-rotate-pulse > div:last-child {
            position: absolute;
            width: 50px;
            height: 50px;
            left: -16px;
            top: -2px;
            background: 0 0;
            border: 2px solid;
            -webkit-animation: ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
            animation: ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
            -webkit-animation-duration: 1s;
            animation-duration: 1s
        }

        @-webkit-keyframes ball-clip-rotate-pulse-rotate {
            0% {
                -webkit-transform: rotate(0) scale(1);
                transform: rotate(0) scale(1)
            }
            50% {
                -webkit-transform: rotate(180deg) scale(.6);
                transform: rotate(180deg) scale(.6)
            }
            100% {
                -webkit-transform: rotate(360deg) scale(1);
                transform: rotate(360deg) scale(1)
            }
        }

        @keyframes ball-clip-rotate-pulse-rotate {
            0% {
                -webkit-transform: rotate(0) scale(1);
                transform: rotate(0) scale(1)
            }
            50% {
                -webkit-transform: rotate(180deg) scale(.6);
                transform: rotate(180deg) scale(.6)
            }
            100% {
                -webkit-transform: rotate(360deg) scale(1);
                transform: rotate(360deg) scale(1)
            }
        }

        @-webkit-keyframes ball-clip-rotate-pulse-scale {
            30% {
                -webkit-transform: scale(.3);
                transform: scale(.3)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes ball-clip-rotate-pulse-scale {
            30% {
                -webkit-transform: scale(.3);
                transform: scale(.3)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @-webkit-keyframes square-spin {
            25% {
                -webkit-transform: perspective(100px) rotateX(180deg) rotateY(0);
                transform: perspective(100px) rotateX(180deg) rotateY(0)
            }
            50% {
                -webkit-transform: perspective(100px) rotateX(180deg) rotateY(180deg);
                transform: perspective(100px) rotateX(180deg) rotateY(180deg)
            }
            75% {
                -webkit-transform: perspective(100px) rotateX(0) rotateY(180deg);
                transform: perspective(100px) rotateX(0) rotateY(180deg)
            }
            100% {
                -webkit-transform: perspective(100px) rotateX(0) rotateY(0);
                transform: perspective(100px) rotateX(0) rotateY(0)
            }
        }

        @keyframes square-spin {
            25% {
                -webkit-transform: perspective(100px) rotateX(180deg) rotateY(0);
                transform: perspective(100px) rotateX(180deg) rotateY(0)
            }
            50% {
                -webkit-transform: perspective(100px) rotateX(180deg) rotateY(180deg);
                transform: perspective(100px) rotateX(180deg) rotateY(180deg)
            }
            75% {
                -webkit-transform: perspective(100px) rotateX(0) rotateY(180deg);
                transform: perspective(100px) rotateX(0) rotateY(180deg)
            }
            100% {
                -webkit-transform: perspective(100px) rotateX(0) rotateY(0);
                transform: perspective(100px) rotateX(0) rotateY(0)
            }
        }

        .square-spin {
            display: inline-block
        }

        .square-spin > div {
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            width: 50px;
            height: 50px;
            -webkit-animation: square-spin 3s 0s cubic-bezier(.09, .57, .49, .9) infinite;
            animation: square-spin 3s 0s cubic-bezier(.09, .57, .49, .9) infinite
        }

        .cube-transition {
            position: relative;
            -webkit-transform: translate(-25px, -25px);
            -ms-transform: translate(-25px, -25px);
            transform: translate(-25px, -25px);
            display: inline-block
        }

        .cube-transition > div {
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            width: 15px;
            height: 15px;
            position: absolute;
            top: -5px;
            left: -5px;
            -webkit-animation: cube-transition 1.6s 0s infinite ease-in-out;
            animation: cube-transition 1.6s 0s infinite ease-in-out
        }

        .cube-transition > div:last-child {
            -webkit-animation-delay: -.8s;
            animation-delay: -.8s
        }

        @-webkit-keyframes cube-transition {
            25% {
                -webkit-transform: translateX(50px) scale(.5) rotate(-90deg);
                transform: translateX(50px) scale(.5) rotate(-90deg)
            }
            50% {
                -webkit-transform: translate(50px, 50px) rotate(-180deg);
                transform: translate(50px, 50px) rotate(-180deg)
            }
            75% {
                -webkit-transform: translateY(50px) scale(.5) rotate(-270deg);
                transform: translateY(50px) scale(.5) rotate(-270deg)
            }
            100% {
                -webkit-transform: rotate(-360deg);
                transform: rotate(-360deg)
            }
        }

        @keyframes cube-transition {
            25% {
                -webkit-transform: translateX(50px) scale(.5) rotate(-90deg);
                transform: translateX(50px) scale(.5) rotate(-90deg)
            }
            50% {
                -webkit-transform: translate(50px, 50px) rotate(-180deg);
                transform: translate(50px, 50px) rotate(-180deg)
            }
            75% {
                -webkit-transform: translateY(50px) scale(.5) rotate(-270deg);
                transform: translateY(50px) scale(.5) rotate(-270deg)
            }
            100% {
                -webkit-transform: rotate(-360deg);
                transform: rotate(-360deg)
            }
        }

        .ball-scale > div {
            border-radius: 100%;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            height: 60px;
            width: 60px;
            -webkit-animation: ball-scale 1s 0s ease-in-out infinite;
            animation: ball-scale 1s 0s ease-in-out infinite
        }

        .ball-scale-multiple > div, .line-scale > div {
            -webkit-animation-fill-mode: both;
            height: 50px
        }

        @-webkit-keyframes ball-scale {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }

        @keyframes ball-scale {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }

        .line-scale > div {
            animation-fill-mode: both;
            width: 5px;
            border-radius: 2px
        }

        .line-scale > div:nth-child(1) {
            -webkit-animation: line-scale 1s -.5s infinite cubic-bezier(.2, .68, .18, 1.08);
            animation: line-scale 1s -.5s infinite cubic-bezier(.2, .68, .18, 1.08)
        }

        .line-scale > div:nth-child(2) {
            -webkit-animation: line-scale 1s -.4s infinite cubic-bezier(.2, .68, .18, 1.08);
            animation: line-scale 1s -.4s infinite cubic-bezier(.2, .68, .18, 1.08)
        }

        .line-scale > div:nth-child(3) {
            -webkit-animation: line-scale 1s -.3s infinite cubic-bezier(.2, .68, .18, 1.08);
            animation: line-scale 1s -.3s infinite cubic-bezier(.2, .68, .18, 1.08)
        }

        .line-scale > div:nth-child(4) {
            -webkit-animation: line-scale 1s -.2s infinite cubic-bezier(.2, .68, .18, 1.08);
            animation: line-scale 1s -.2s infinite cubic-bezier(.2, .68, .18, 1.08)
        }

        .line-scale > div:nth-child(5) {
            -webkit-animation: line-scale 1s -.1s infinite cubic-bezier(.2, .68, .18, 1.08);
            animation: line-scale 1s -.1s infinite cubic-bezier(.2, .68, .18, 1.08)
        }

        @-webkit-keyframes line-scale {
            0%, 100% {
                -webkit-transform: scaley(1);
                transform: scaley(1)
            }
            50% {
                -webkit-transform: scaley(.4);
                transform: scaley(.4)
            }
        }

        @keyframes line-scale {
            0%, 100% {
                -webkit-transform: scaley(1);
                transform: scaley(1)
            }
            50% {
                -webkit-transform: scaley(.4);
                transform: scaley(.4)
            }
        }

        .ball-scale-multiple {
            position: relative;
            -webkit-transform: translateY(30px);
            -ms-transform: translateY(30px);
            transform: translateY(30px);
            display: inline-block
        }

        .ball-scale-multiple > div {
            border-radius: 100%;
            animation-fill-mode: both;
            margin: 0;
            position: absolute;
            left: -30px;
            top: 0;
            opacity: 0;
            width: 50px;
            -webkit-animation: ball-scale-multiple 1s 0s linear infinite;
            animation: ball-scale-multiple 1s 0s linear infinite
        }

        .ball-scale-multiple > div:nth-child(2), .ball-scale-multiple > div:nth-child(3) {
            -webkit-animation-delay: -.2s;
            animation-delay: -.2s
        }

        @-webkit-keyframes ball-scale-multiple {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
                opacity: 0
            }
            5% {
                opacity: 1
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }

        @keyframes ball-scale-multiple {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0);
                opacity: 0
            }
            5% {
                opacity: 1
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }

        .ball-pulse-sync {
            display: inline-block
        }

        .ball-pulse-sync > div {
            width: 15px;
            height: 15px;
            border-radius: 100%;
            margin: 2px;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            display: inline-block
        }

        .ball-pulse-sync > div:nth-child(1) {
            -webkit-animation: ball-pulse-sync .6s -.21s infinite ease-in-out;
            animation: ball-pulse-sync .6s -.21s infinite ease-in-out
        }

        .ball-pulse-sync > div:nth-child(2) {
            -webkit-animation: ball-pulse-sync .6s -.14s infinite ease-in-out;
            animation: ball-pulse-sync .6s -.14s infinite ease-in-out
        }

        .ball-pulse-sync > div:nth-child(3) {
            -webkit-animation: ball-pulse-sync .6s -70ms infinite ease-in-out;
            animation: ball-pulse-sync .6s -70ms infinite ease-in-out
        }

        @-webkit-keyframes ball-pulse-sync {
            33% {
                -webkit-transform: translateY(10px);
                transform: translateY(10px)
            }
            66% {
                -webkit-transform: translateY(-10px);
                transform: translateY(-10px)
            }
            100% {
                -webkit-transform: translateY(0);
                transform: translateY(0)
            }
        }

        @keyframes ball-pulse-sync {
            33% {
                -webkit-transform: translateY(10px);
                transform: translateY(10px)
            }
            66% {
                -webkit-transform: translateY(-10px);
                transform: translateY(-10px)
            }
            100% {
                -webkit-transform: translateY(0);
                transform: translateY(0)
            }
        }

        .transparent-circle {
            display: inline-block;
            border-top: .5em solid rgba(255, 255, 255, .2);
            border-right: .5em solid rgba(255, 255, 255, .2);
            border-bottom: .5em solid rgba(255, 255, 255, .2);
            border-left: .5em solid #fff;
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-animation: transparent-circle 1.1s infinite linear;
            animation: transparent-circle 1.1s infinite linear;
            width: 50px;
            height: 50px;
            border-radius: 50%
        }

        .transparent-circle:after {
            border-radius: 50%;
            width: 10em;
            height: 10em
        }

        @-webkit-keyframes transparent-circle {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        @keyframes transparent-circle {
            0% {
                -webkit-transform: rotate(0);
                transform: rotate(0)
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg)
            }
        }

        .ball-spin-fade-loader {
            position: relative;
            top: -10px;
            left: -10px;
            display: inline-block
        }

        .ball-spin-fade-loader > div {
            width: 15px;
            height: 15px;
            border-radius: 100%;
            margin: 2px;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            position: absolute;
            -webkit-animation: ball-spin-fade-loader 1s infinite linear;
            animation: ball-spin-fade-loader 1s infinite linear
        }

        .ball-spin-fade-loader > div:nth-child(1) {
            top: 25px;
            left: 0;
            animation-delay: -.84s;
            -webkit-animation-delay: -.84s
        }

        .ball-spin-fade-loader > div:nth-child(2) {
            top: 17.05px;
            left: 17.05px;
            animation-delay: -.72s;
            -webkit-animation-delay: -.72s
        }

        .ball-spin-fade-loader > div:nth-child(3) {
            top: 0;
            left: 25px;
            animation-delay: -.6s;
            -webkit-animation-delay: -.6s
        }

        .ball-spin-fade-loader > div:nth-child(4) {
            top: -17.05px;
            left: 17.05px;
            animation-delay: -.48s;
            -webkit-animation-delay: -.48s
        }

        .ball-spin-fade-loader > div:nth-child(5) {
            top: -25px;
            left: 0;
            animation-delay: -.36s;
            -webkit-animation-delay: -.36s
        }

        .ball-spin-fade-loader > div:nth-child(6) {
            top: -17.05px;
            left: -17.05px;
            animation-delay: -.24s;
            -webkit-animation-delay: -.24s
        }

        .ball-spin-fade-loader > div:nth-child(7) {
            top: 0;
            left: -25px;
            animation-delay: -.12s;
            -webkit-animation-delay: -.12s
        }

        .ball-spin-fade-loader > div:nth-child(8) {
            top: 17.05px;
            left: -17.05px;
            animation-delay: 0s;
            -webkit-animation-delay: 0s
        }

        @-webkit-keyframes ball-spin-fade-loader {
            50% {
                opacity: .3;
                -webkit-transform: scale(.4);
                transform: scale(.4)
            }
            100% {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }

        @keyframes ball-spin-fade-loader {
            50% {
                opacity: .3;
                -webkit-transform: scale(.4);
                transform: scale(.4)
            }
            100% {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1)
            }
        }</style>
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <link rel="alternate" type="application/rss+xml" title="Fortuna Template - Jupiter WordPress Theme &raquo; Feed"
          href="http://demos.artbees.net/jupiter5/fortuna/feed/"/>
    <link rel="alternate" type="application/rss+xml"
          title="Fortuna Template - Jupiter WordPress Theme &raquo; Comments Feed"
          href="http://demos.artbees.net/jupiter5/fortuna/comments/feed/"/>

    <link rel="shortcut icon"
          href="http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/2016/01/favicon.png"/>
    <script type="text/javascript">
      window.abb = {};
      php = {};
      window.PHP = {};
      PHP.ajax = "http://demos.artbees.net/jupiter5/fortuna/wp-admin/admin-ajax.php";
      PHP.wp_p_id = "67";
      var mk_header_parallax, mk_banner_parallax, mk_page_parallax, mk_footer_parallax, mk_body_parallax;
      var mk_images_dir = "http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/images",
          mk_theme_js_path = "http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js",
          mk_theme_dir = "http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter",
          mk_captcha_placeholder = "Enter Captcha",
          mk_captcha_invalid_txt = "Invalid. Try again.",
          mk_captcha_correct_txt = "Captcha correct.",
          mk_responsive_nav_width = 1200,
          mk_vertical_header_back = "Back",
          mk_vertical_header_anim = "1",
          mk_check_rtl = true,
          mk_grid_width = 1200,
          mk_ajax_search_option = "fullscreen_search",
          mk_preloader_bg_color = "#ffffff",
          mk_accent_color = "#1d161e",
          mk_go_to_top = "false",
          mk_smooth_scroll = "true",
          mk_preloader_bar_color = "#1d161e",
          mk_preloader_logo = "";
      var mk_header_parallax = false,
          mk_banner_parallax = false,
          mk_footer_parallax = false,
          mk_body_parallax = false,
          mk_no_more_posts = "No More Posts";
    </script>
    <link rel='stylesheet' id='contact-form-7-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='theme-styles-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/min/core-styles.css?ver=5.9.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='google-font-api-special-1-css'
          href='http://fonts.googleapis.com/css?family=Playfair+Display%3A100italic%2C200italic%2C300italic%2C400italic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic%2C100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.7.4'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='js_composer_front-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/css/js_composer.min.css?ver=5.1.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='global-assets-css-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/min/components-full.css?ver=5.9.1'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='theme-options-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/mk_assets/theme-options-production.css?ver=1492671791'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='mk-style-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/style.css?ver=4.7.4' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='theme-dynamic-styles-css'
          href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/custom.css?ver=4.7.4' type='text/css'
          media='all'/>
    <style id='theme-dynamic-styles-inline-css' type='text/css'>
        body {
            background-color: #fff;
        }

        .mk-header {
            background-color: #f7f7f7;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
        }

        .mk-header-bg {
            background-color: #1d161e;
        }

        .mk-classic-nav-bg {
            background-color: #1d161e;
        }

        .master-holder-bg {
            background-color: #fff;
        }

        #mk-footer {
            background-color: #3d4045;
        }

        #mk-boxed-layout {
            -webkit-box-shadow: 0 0 0px rgba(0, 0, 0, 0);
            -moz-box-shadow: 0 0 0px rgba(0, 0, 0, 0);
            box-shadow: 0 0 0px rgba(0, 0, 0, 0);
        }

        .mk-news-tab .mk-tabs-tabs .is-active a, .mk-fancy-title.pattern-style span, .mk-fancy-title.pattern-style.color-gradient span:after, .page-bg-color {
            background-color: #fff;
        }

        .page-title {
            font-size: 20px;
            color: #4d4d4d;
            text-transform: uppercase;
            font-weight: 400;
            letter-spacing: 2px;
        }

        .page-subtitle {
            font-size: 14px;
            line-height: 100%;
            color: #a3a3a3;
            font-size: 14px;
            text-transform: none;
        }

        .header-style-1 .mk-header-padding-wrapper, .header-style-2 .mk-header-padding-wrapper, .header-style-3 .mk-header-padding-wrapper {
            padding-top: 91px;
        }

        @font-face {
            font-family: 'star';
            src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot');
            src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot?#iefix') format('embedded-opentype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.woff') format('woff'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.ttf') format('truetype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.svg#star') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'WooCommerce';
            src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot');
            src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot?#iefix') format('embedded-opentype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.woff') format('woff'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.ttf') format('truetype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.svg#WooCommerce') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        #mk-ornamental-title-3 {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #mk-ornamental-title-3 .title {
            color: #ffffff;
            font-weight: bold;
            font-style: inherit;
            text-transform: uppercase;
        }

        #mk-ornamental-title-3 .title span::after, #mk-ornamental-title-3 .title span::before {
            border-top: 1px solid #ffffff;
        }

        #mk-ornamental-title-3 .title {
            font-size: 80px;
            line-height: 86px;
        }

        #mk-ornamental-title-3 .title span::after, #mk-ornamental-title-3 .title span::before {
            top: 42.5px;
        }

        #mk-shape-divider-4 .shape__container {
            background-color:;
        }

        #mk-shape-divider-4 .shape__container .shape {
            overflow: hidden;
            height:;
        }

        #mk-shape-divider-4 .shape__container .shape svg {
            position: relative;
            top: 0.6px;
        }

        #mk-shape-divider-4 .shape__container .shape .speech-left, #mk-shape-divider-4 .shape__container .shape .speech-right {
            background-color: #ffffff;
        }

        .full-width-2 .mk-video-color-mask {
            background: #e3aa7f;
            background: -moz-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e3aa7f), color-stop(100%, #26001f));
            background: -webkit-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
            background: -o-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
            background: -ms-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
            background: linear-gradient(to bottom, #e3aa7f 0%, #26001f 100%);
        }

        .full-width-2 {
            min-height: 450px;
            margin-bottom: 0px;
        }

        .full-width-2 .page-section-content {
            padding: 0px 0 100px;
        }

        #background-layer--2 {;
            background-position: center top;
            background-repeat: no-repeat;;
        }

        #background-layer--2 .mk-color-layer {;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        .full-width-2 .mk-skip-to-next {
            bottom: 100px;
        }

        #padding-5 {
            height: 100px;
        }

        #fancy-title-6 {
            letter-spacing: 0px;
            text-transform: uppercase;
            font-size: 35px;
            color: #c95a66;
            text-align: center;
            font-style: inherit;
            font-weight: 400;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        #fancy-title-6 span {
        }

        @media handheld, only screen and (max-width: 767px) {
            #fancy-title-6 {
                text-align: center !important;
            }
        }

        #mk-ornamental-title-7 {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        #mk-ornamental-title-7 .title {
            color: #888888;
            font-weight: inherit;
            font-style: inherit;
            text-transform: capitalize;
        }

        #mk-ornamental-title-7 .title span::after, #mk-ornamental-title-7 .title span::before {
            border-top: 1px solid #888888;
        }

        #mk-ornamental-title-7 .title {
            font-size: 25px;
            line-height: 31px;
        }

        #mk-ornamental-title-7 .title span::after, #mk-ornamental-title-7 .title span::before {
            top: 15px;
        }

        #text-block-8 {
            margin-bottom: 0px;
            text-align: center;
        }

        #padding-9 {
            height: 50px;
        }

        #padding-10 {
            height: 100px;
        }

        #mk-shape-divider-12 .shape__container {
            background-color:;
        }

        #mk-shape-divider-12 .shape__container .shape {
            overflow: hidden;
            height:;
        }

        #mk-shape-divider-12 .shape__container .shape svg {
            position: relative;
            top: -0.6px;
        }

        #mk-shape-divider-12 .shape__container .shape .speech-left, #mk-shape-divider-12 .shape__container .shape .speech-right {
            background-color: #ffffff;
        }

        #padding-13 {
            height: 145px;
        }

        #mk-ornamental-title-15 {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #mk-ornamental-title-15 .title {
            color: #222222;
            font-weight: inherit;
            font-style: inherit;
            text-transform: capitalize;
        }

        #mk-ornamental-title-15 .title span::after, #mk-ornamental-title-15 .title span::before {
            border-top: 1px solid #222222;
        }

        #mk-ornamental-title-15 .title {
            font-size: 35px;
            line-height: 41px;
        }

        #mk-ornamental-title-15 .title span::after, #mk-ornamental-title-15 .title span::before {
            top: 20px;
        }

        #fancy-title-16 {
            letter-spacing: 0px;
            text-transform: uppercase;
            font-size: 35px;
            color: #222222;
            text-align: center;
            font-style: inherit;
            font-weight: 400;
            padding-top: 0px;
            padding-bottom: px;
        }

        #fancy-title-16 span {
        }

        @media handheld, only screen and (max-width: 767px) {
            #fancy-title-16 {
                text-align: center !important;
            }
        }

        #text-block-17 {
            margin-bottom: 30px;
            text-align: center;
        }

        #mk-ornamental-title-18 {
            margin-top: 0px;
            margin-bottom: 20px;
        }

        #mk-ornamental-title-18 .title {
            color: #44c6d5;
            font-weight: bold;
            font-style: inherit;
            text-transform: uppercase;
        }

        #mk-ornamental-title-18 .title span::after, #mk-ornamental-title-18 .title span::before {
            border-top: 1px solid #44c6d5;
        }

        #mk-ornamental-title-18 .title {
            font-size: 14px;
            line-height: 20px;
        }

        #mk-ornamental-title-18 .title span::after {
            border-top: 1px solid #44c6d5;
        }

        #mk-ornamental-title-18 .title span::before {
            border-top: 1px solid #44c6d5;
        }

        #box-14 .box-holder {
            border: 5px solid #f5f1c4;
        }

        #box-14 .box-holder {
            background-color: rgba(255, 255, 255, 0.8);
        }

        #box-14 {
            margin-bottom: 0px;
        }

        #box-14 .box-holder {
            min-height: 100px;
            padding: 70px 60px;
        }

        #box-14 .box-holder:hover {
            background-color:;
        }

        #padding-19 {
            height: 40px;
        }

        .full-width-11 {
            min-height: 780px;
            margin-bottom: 0px;
        }

        .full-width-11 .page-section-content {
            padding: 100px 0 0px;
        }

        #background-layer--11 {;
            background-position: center top;
            background-repeat: no-repeat;;
        }

        #background-layer--11 .mk-color-layer {;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }

        #wpcf7-f511-p143-o1 .vc_row . wpb_column:first-child {
            padding-right: 15px;
        }

        #wpcf7-f511-p143-o1 .vc_row . wpb_column:last-child {
            padding-left: 15px;
        }

        #wpcf7-f511-p143-o1 input[type=text] {
            border: 0;
            background-color: #aa8dd3;
            height: 40px;
            line-height: 40px;
            width: 100%;
            margin-bottom: 20px;
            font-size: 14px;
            color: #222222 !important;
        }

        #wpcf7-f511-p143-o1 label {
            font-size: 16px;
            margin-bottom: 10px;
            text-align: left;
        }

        #wpcf7-f511-p143-o1 input[type=file] {
            margin-bottom: 20px;
        }

        #wpcf7-f511-p143-o1 textarea {
            border: 0;
            background-color: #aa8dd3;
            height: 125px;
            width: 100%;
            margin-bottom: 10px;
            font-size: 14px;
            resize: none;
            Color: #222222 !important;
        }

        #wpcf7-f511-p143-o1 input[type=submit] {
            padding: 15px 40px;
            background-color: #00ffff;
            border: 0;
            color: #623e95;
            margin-top: 15px;
            font-size: 14px;
        }

        @media handheld, only screen and (max-width: 767px) {
            #wpcf7-f511-p143-o1 .vc_row . wpb_column:first-child {
                padding-right: 0 !important;
            }

            #wpcf7-f511-p143-o1 .vc_row . wpb_column:last-child {
                padding-left: 0 !important;
            }
        }
    </style>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript'
            src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <link rel='https://api.w.org/' href='http://demos.artbees.net/jupiter5/fortuna/wp-json/'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="http://demos.artbees.net/jupiter5/fortuna/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://demos.artbees.net/jupiter5/fortuna/wp-includes/wlwmanifest.xml"/>
    <meta name="generator" content="WordPress 4.7.4"/>
    <link rel="canonical" href="http://demos.artbees.net/jupiter5/fortuna/reservation/"/>
    <link rel='shortlink' href='http://demos.artbees.net/jupiter5/fortuna/?p=67'/>
    <link rel="alternate" type="application/json+oembed"
          href="http://demos.artbees.net/jupiter5/fortuna/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemos.artbees.net%2Fjupiter5%2Ffortuna%2Freservation%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="http://demos.artbees.net/jupiter5/fortuna/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemos.artbees.net%2Fjupiter5%2Ffortuna%2Freservation%2F&#038;format=xml"/>
    <script> var isTest = false; </script>
    <style id="js-media-query-css">.mk-event-countdown-ul:media( max-width: 750px ) li {
            width: 90%;
            display: block;
            margin: 0 auto 15px
        }

        .mk-process-steps:media( max-width: 960px ) ul:before {
            display: none !important
        }

        .mk-process-steps:media( max-width: 960px ) li {
            margin-bottom: 30px !important;
            width: 100% !important;
            text-align: center
        }</style>
    <meta itemprop="author" content=""/>
    <meta itemprop="datePublished" content="January 14, 2016"/>
    <meta itemprop="dateModified" content="January 15, 2016"/>
    <meta itemprop="publisher" content="Fortuna Template - Jupiter WordPress Theme"/>
    <style type="text/css">.recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }</style>
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/css/vc_lte_ie9.min.css"
          media="screen"><![endif]-->
    <meta name="generator" content="Jupiter 5.9.1"/>
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
    <link rel="stylesheet" href="main.css">

</head>