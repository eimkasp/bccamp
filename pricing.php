<div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-false  attched-false     js-master-row  mk-in-viewport pricing-wrapper">
  <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full"></div>
  <div style="" class="vc_col-sm-8 wpb_column column_container  _ height-full">
    <h2 id="fancy-title-9" class="mk-fancy-title  simple-style  color-single">
      <span>Camp dates</span>
    </h2>

    <span class="description-pricing text-center">
          All packages has same training schedule and other extras included. Packages are different only by accomodation type
        </span>

    <div id="mk-pricing-table-28" class="shortcode pricing-table   no-pricing-offer new-style">
      <ul class="pricing-cols">

        <li class="pricing-col three-table drop-shadow-false column-bigger-false" style="border:1px solid #fff">
          <div class="pricing-heading">
            <div class="pricing-plan style-light" style="background-color:#f1c40f;">1 Star Package</div>
            <div class="pricing-price style-light" style="background-color:#f1c40f;"><span><sup>€</sup>51<sub>/ Per person / per day</sub></span>
            </div>
          </div>
          <div class="pricing-features style-light" style="background-color:#f1c40f;"></div>
          <div class="pricing-button style-light" style="background-color:#f1c40f;">
            <div id="mk-button-30" class="mk-button-container _ relative    block text-center ">
              <div class="where-stay">
                Motel/dorm accomodation <br>
                All meals included <br>
                All training sessions included
              </div>
            </div>
            <div id="mk-button-30" class="mk-button-container _ relative    block text-center ">
              <a href="packageinfo.php" class="view-button">More Info</a>
            </div>
            <div class="clearboth"></div>
          </div>
        </li>
        <li class="pricing-col three-table drop-shadow-false column-bigger-false" style="border:1px solid #fff">
          <div class="pricing-heading">
            <div class="pricing-plan style-light" style="background-color:#27ae60;">3 Star Package</div>
            <div class="pricing-price style-light" style="background-color:#27ae60;"><span><sup>€</sup>60<sub>/ Per person / per day</sub></span>
            </div>
          </div>
          <div class="pricing-features style-light" style="background-color:#27ae60;"></div>
          <div class="pricing-button style-light" style="background-color:#27ae60;">
            <div class="where-stay">
              3 star hotel<br>
              All meals included <br>
              All training sessions included

            </div>
            <div id="mk-button-30" class="mk-button-container _ relative    block text-center ">
              <a href="packageinfo.php" class="view-button">More Info</a>
            </div>
            <div class="clearboth"></div>
          </div>
        </li>
        <li class="pricing-col three-table drop-shadow-false column-bigger-false" style="border:1px solid #fff">
          <div class="pricing-heading">
            <div class="pricing-plan style-light" style="background-color:#e74c3c;">5 Star Package</div>
            <div class="pricing-price style-light" style="background-color:#e74c3c;"><span><sup>€</sup>80<sub>/ Per person / per day</sub></span>
            </div>
          </div>
          <div class="pricing-features style-light" style="background-color:#e74c3c;"></div>
          <div class="pricing-button style-light" style="background-color:#e74c3c;">
            <div class="where-stay">
              4+ stars accomodation
              All meals included <br>
              All training sessions included
            </div>
            <div id="mk-button-30" class="mk-button-container _ relative    block text-center ">
              <a href="packageinfo.php" class="view-button">More Info</a>
            </div>
            <div class="clearboth"></div>
          </div>
        </li>
        <br>

      </ul>
      <h3 style="padding: 80px 0 0px;" class="text-center">Camp dates</h3>
      <ul class="pricing-cols">


          <?php
          $dates = [
              "From: 2017-06-12 <br> To: 2017-06-18",
              "From: 2017-07-17 <br> To: 2017-07-23",
              "From: 2017-07-24 <br> To: 2017-07-30",
              "From: 2017-08-01 <br> To: 2017-08-06",
              "From: 2017-08-07 <br> To: 2017-08-13",
              "From: 2017-10-16 <br> To: 2017-10-22",
              "From: 2017-10-23 <br> To: 2017-10-29",
              "From: 2018-01-02 <br> To: 2018-01-07",
              "From: 2018-02-19 <br>To: 2018-02-25",
              "From: 2018-02-26 <br>To: 2018-03-04",
              "From: 2018-04-08 <br>To: 2018-04-15",


          ];
          for ($i = 0; $i < count($dates); $i++) : ?>
            <li class="pricing-col three-table drop-shadow-false column-bigger-false" style="border:1px solid #fff">
              <div class="pricing-heading">
                <div class="pricing-plan style-light"
                     style="background-color:#f1592a; line-height:1.2; font-size: 16px;"><?php echo $dates[$i]; ?>
                </div>

              </div>
              <div class="pricing-features style-light" style="background-color:#f1592a;"></div>
              <div class="pricing-button style-light" style="background-color:#f1592a;">

                <div id="mk-button-29" class="mk-button-container _ relative    block text-center ">

                  <a href="rezervacija.php"
                     target="_self"
                     class="mk-button js-smooth-scroll mk-button--dimension-flat mk-button--size-large mk-button--corner-rounded text-color-light _ relative text-center font-weight-700 no-backface  letter-spacing-2 inline">
                    <span class="mk-button--text">
                      Register
                    </span>
                  </a>
                </div>
                <div class="clearboth"></div>
              </div>
            </li>
          <?php endfor; ?>
        <li class="pricing-col three-table drop-shadow-false column-bigger-false" style="border:1px solid #fff">
          <div class="pricing-heading">
            <div class="pricing-plan style-light"
                 style="background-color:#f1592a; line-height:1.2; font-size: 16px;">Request a <br> different date
            </div>

          </div>
          <div class="pricing-features style-light" style="background-color:#f1592a;"></div>
          <div class="pricing-button style-light" style="background-color:#f1592a;">

            <div id="mk-button-29" class="mk-button-container _ relative    block text-center ">

              <a href="#!"
                 target="_self"
                 class="mk-button js-smooth-scroll mk-button--dimension-flat mk-button--size-large mk-button--corner-rounded text-color-light _ relative text-center font-weight-700 no-backface  letter-spacing-2 inline">
                    <span class="mk-button--text">
                      Contact us
                    </span>
              </a>
            </div>
            <div class="clearboth"></div>
          </div>
        </li>

      </ul>
    </div>
  </div>
  <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full"></div>
</div>