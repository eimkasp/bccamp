<div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-false  attched-false     js-master-row  mk-in-viewport pricing-wrapper">
  <div style="" class="vc_col-sm-1 wpb_column column_container  _ height-full"></div>
  <div style="" class="vc_col-sm-10 wpb_column column_container  _ height-full">
    <h2 id="fancy-title-9" class="mk-fancy-title  simple-style  color-single">
      <span>Accomodation options</span>
    </h2>

    <span class="description-pricing text-center">
      Bellow you can see all available accomodation options while staying at BC Camp
    </span>

    <div class="cols-wrapper">
      <div class="vc_col-sm-4">
        <div class="img-wrapper acom">
          <img src="http://kaunasgrandprix.lt/wp-content/uploads/2017/02/jachtklubas.jpg" />
          <div class="accomodation-desc">
            <h5>Motel/Dormitory</h5>
            <strong>Price: 199€</strong> <br>
            Breakfast included.
          </div>
        </div>
      </div>
      <div class="vc_col-sm-4">
        <div class="img-wrapper acom">
          <div class="overlay-acom">
            <a href="#">
              Hotel website
            </a>

          </div>
            <img src="http://kaunasgrandprix.lt/wp-content/uploads/2017/02/viesbutyje-4fa2594fb6c83.jpg" />
          <div class="accomodation-desc">
            <h5>Hotel 3 stars</h5>
            <strong>Price: 199€</strong> <br>
            Breakfast included.
          </div>
        </div>
      </div>
      <div class="vc_col-sm-4">
        <div class="img-wrapper acom">
          <img src="http://kaunasgrandprix.lt/wp-content/uploads/2017/02/55929490-768x512.jpg" />
          <div class="accomodation-desc">
            <h5>Hotel 4+ stars</h5>
            <strong>Price: 199€</strong> <br>
            Breakfast included.
          </div>
        </div>
      </div>

    </div>

  </div>
  <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full"></div>
</div>