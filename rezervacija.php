<!DOCTYPE html>
<html lang="en-US">
<head>

  <meta charset="UTF-8"/>
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta name="format-detection" content="telephone=no">
  <title>Reservation &#8211; Fortuna Template &#8211; Jupiter WordPress Theme</title>
  <script type="text/javascript">var ajaxurl = "http://demos.artbees.net/jupiter5/fortuna/wp-admin/admin-ajax.php"</script>
  <style id="critical-path-css" type='text/css'>body, html {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0
  }

  .page-preloader {
    top: 0;
    left: 0;
    z-index: 999;
    position: fixed;
    height: 100%;
    width: 100%;
    text-align: center
  }

  .preloader-logo, .preloader-preview-area {
    top: 50%;
    max-height: calc(50% - 20px);
    opacity: 1
  }

  .preloader-preview-area {
    -webkit-animation-delay: -.2s;
    animation-delay: -.2s;
    -webkit-transform: translateY(100%);
    -ms-transform: translateY(100%);
    transform: translateY(100%);
    margin-top: 10px;
    width: 100%;
    text-align: center;
    position: absolute
  }

  .preloader-logo {
    max-width: 90%;
    -webkit-transform: translateY(-100%);
    -ms-transform: translateY(-100%);
    transform: translateY(-100%);
    margin: -10px auto 0;
    position: relative
  }

  .ball-pulse > div, .ball-scale > div, .line-scale > div {
    margin: 2px;
    display: inline-block
  }

  .ball-pulse > div {
    width: 15px;
    height: 15px;
    border-radius: 100%;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    -webkit-animation: ball-pulse .75s infinite cubic-bezier(.2, .68, .18, 1.08);
    animation: ball-pulse .75s infinite cubic-bezier(.2, .68, .18, 1.08)
  }

  .ball-pulse > div:nth-child(1) {
    -webkit-animation-delay: -.36s;
    animation-delay: -.36s
  }

  .ball-pulse > div:nth-child(2) {
    -webkit-animation-delay: -.24s;
    animation-delay: -.24s
  }

  .ball-pulse > div:nth-child(3) {
    -webkit-animation-delay: -.12s;
    animation-delay: -.12s
  }

  @-webkit-keyframes ball-pulse {
    0%, 80% {
      -webkit-transform: scale(1);
      transform: scale(1);
      opacity: 1
    }
    45% {
      -webkit-transform: scale(.1);
      transform: scale(.1);
      opacity: .7
    }
  }

  @keyframes ball-pulse {
    0%, 80% {
      -webkit-transform: scale(1);
      transform: scale(1);
      opacity: 1
    }
    45% {
      -webkit-transform: scale(.1);
      transform: scale(.1);
      opacity: .7
    }
  }

  .ball-clip-rotate-pulse {
    position: relative;
    -webkit-transform: translateY(-15px) translateX(-10px);
    -ms-transform: translateY(-15px) translateX(-10px);
    transform: translateY(-15px) translateX(-10px);
    display: inline-block
  }

  .ball-clip-rotate-pulse > div {
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    position: absolute;
    top: 0;
    left: 0;
    border-radius: 100%
  }

  .ball-clip-rotate-pulse > div:first-child {
    height: 36px;
    width: 36px;
    top: 7px;
    left: -7px;
    -webkit-animation: ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
    animation: ball-clip-rotate-pulse-scale 1s 0s cubic-bezier(.09, .57, .49, .9) infinite
  }

  .ball-clip-rotate-pulse > div:last-child {
    position: absolute;
    width: 50px;
    height: 50px;
    left: -16px;
    top: -2px;
    background: 0 0;
    border: 2px solid;
    -webkit-animation: ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
    animation: ball-clip-rotate-pulse-rotate 1s 0s cubic-bezier(.09, .57, .49, .9) infinite;
    -webkit-animation-duration: 1s;
    animation-duration: 1s
  }

  @-webkit-keyframes ball-clip-rotate-pulse-rotate {
    0% {
      -webkit-transform: rotate(0) scale(1);
      transform: rotate(0) scale(1)
    }
    50% {
      -webkit-transform: rotate(180deg) scale(.6);
      transform: rotate(180deg) scale(.6)
    }
    100% {
      -webkit-transform: rotate(360deg) scale(1);
      transform: rotate(360deg) scale(1)
    }
  }

  @keyframes ball-clip-rotate-pulse-rotate {
    0% {
      -webkit-transform: rotate(0) scale(1);
      transform: rotate(0) scale(1)
    }
    50% {
      -webkit-transform: rotate(180deg) scale(.6);
      transform: rotate(180deg) scale(.6)
    }
    100% {
      -webkit-transform: rotate(360deg) scale(1);
      transform: rotate(360deg) scale(1)
    }
  }

  @-webkit-keyframes ball-clip-rotate-pulse-scale {
    30% {
      -webkit-transform: scale(.3);
      transform: scale(.3)
    }
    100% {
      -webkit-transform: scale(1);
      transform: scale(1)
    }
  }

  @keyframes ball-clip-rotate-pulse-scale {
    30% {
      -webkit-transform: scale(.3);
      transform: scale(.3)
    }
    100% {
      -webkit-transform: scale(1);
      transform: scale(1)
    }
  }

  @-webkit-keyframes square-spin {
    25% {
      -webkit-transform: perspective(100px) rotateX(180deg) rotateY(0);
      transform: perspective(100px) rotateX(180deg) rotateY(0)
    }
    50% {
      -webkit-transform: perspective(100px) rotateX(180deg) rotateY(180deg);
      transform: perspective(100px) rotateX(180deg) rotateY(180deg)
    }
    75% {
      -webkit-transform: perspective(100px) rotateX(0) rotateY(180deg);
      transform: perspective(100px) rotateX(0) rotateY(180deg)
    }
    100% {
      -webkit-transform: perspective(100px) rotateX(0) rotateY(0);
      transform: perspective(100px) rotateX(0) rotateY(0)
    }
  }

  @keyframes square-spin {
    25% {
      -webkit-transform: perspective(100px) rotateX(180deg) rotateY(0);
      transform: perspective(100px) rotateX(180deg) rotateY(0)
    }
    50% {
      -webkit-transform: perspective(100px) rotateX(180deg) rotateY(180deg);
      transform: perspective(100px) rotateX(180deg) rotateY(180deg)
    }
    75% {
      -webkit-transform: perspective(100px) rotateX(0) rotateY(180deg);
      transform: perspective(100px) rotateX(0) rotateY(180deg)
    }
    100% {
      -webkit-transform: perspective(100px) rotateX(0) rotateY(0);
      transform: perspective(100px) rotateX(0) rotateY(0)
    }
  }

  .square-spin {
    display: inline-block
  }

  .square-spin > div {
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    width: 50px;
    height: 50px;
    -webkit-animation: square-spin 3s 0s cubic-bezier(.09, .57, .49, .9) infinite;
    animation: square-spin 3s 0s cubic-bezier(.09, .57, .49, .9) infinite
  }

  .cube-transition {
    position: relative;
    -webkit-transform: translate(-25px, -25px);
    -ms-transform: translate(-25px, -25px);
    transform: translate(-25px, -25px);
    display: inline-block
  }

  .cube-transition > div {
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    width: 15px;
    height: 15px;
    position: absolute;
    top: -5px;
    left: -5px;
    -webkit-animation: cube-transition 1.6s 0s infinite ease-in-out;
    animation: cube-transition 1.6s 0s infinite ease-in-out
  }

  .cube-transition > div:last-child {
    -webkit-animation-delay: -.8s;
    animation-delay: -.8s
  }

  @-webkit-keyframes cube-transition {
    25% {
      -webkit-transform: translateX(50px) scale(.5) rotate(-90deg);
      transform: translateX(50px) scale(.5) rotate(-90deg)
    }
    50% {
      -webkit-transform: translate(50px, 50px) rotate(-180deg);
      transform: translate(50px, 50px) rotate(-180deg)
    }
    75% {
      -webkit-transform: translateY(50px) scale(.5) rotate(-270deg);
      transform: translateY(50px) scale(.5) rotate(-270deg)
    }
    100% {
      -webkit-transform: rotate(-360deg);
      transform: rotate(-360deg)
    }
  }

  @keyframes cube-transition {
    25% {
      -webkit-transform: translateX(50px) scale(.5) rotate(-90deg);
      transform: translateX(50px) scale(.5) rotate(-90deg)
    }
    50% {
      -webkit-transform: translate(50px, 50px) rotate(-180deg);
      transform: translate(50px, 50px) rotate(-180deg)
    }
    75% {
      -webkit-transform: translateY(50px) scale(.5) rotate(-270deg);
      transform: translateY(50px) scale(.5) rotate(-270deg)
    }
    100% {
      -webkit-transform: rotate(-360deg);
      transform: rotate(-360deg)
    }
  }

  .ball-scale > div {
    border-radius: 100%;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    height: 60px;
    width: 60px;
    -webkit-animation: ball-scale 1s 0s ease-in-out infinite;
    animation: ball-scale 1s 0s ease-in-out infinite
  }

  .ball-scale-multiple > div, .line-scale > div {
    -webkit-animation-fill-mode: both;
    height: 50px
  }

  @-webkit-keyframes ball-scale {
    0% {
      -webkit-transform: scale(0);
      transform: scale(0)
    }
    100% {
      -webkit-transform: scale(1);
      transform: scale(1);
      opacity: 0
    }
  }

  @keyframes ball-scale {
    0% {
      -webkit-transform: scale(0);
      transform: scale(0)
    }
    100% {
      -webkit-transform: scale(1);
      transform: scale(1);
      opacity: 0
    }
  }

  .line-scale > div {
    animation-fill-mode: both;
    width: 5px;
    border-radius: 2px
  }

  .line-scale > div:nth-child(1) {
    -webkit-animation: line-scale 1s -.5s infinite cubic-bezier(.2, .68, .18, 1.08);
    animation: line-scale 1s -.5s infinite cubic-bezier(.2, .68, .18, 1.08)
  }

  .line-scale > div:nth-child(2) {
    -webkit-animation: line-scale 1s -.4s infinite cubic-bezier(.2, .68, .18, 1.08);
    animation: line-scale 1s -.4s infinite cubic-bezier(.2, .68, .18, 1.08)
  }

  .line-scale > div:nth-child(3) {
    -webkit-animation: line-scale 1s -.3s infinite cubic-bezier(.2, .68, .18, 1.08);
    animation: line-scale 1s -.3s infinite cubic-bezier(.2, .68, .18, 1.08)
  }

  .line-scale > div:nth-child(4) {
    -webkit-animation: line-scale 1s -.2s infinite cubic-bezier(.2, .68, .18, 1.08);
    animation: line-scale 1s -.2s infinite cubic-bezier(.2, .68, .18, 1.08)
  }

  .line-scale > div:nth-child(5) {
    -webkit-animation: line-scale 1s -.1s infinite cubic-bezier(.2, .68, .18, 1.08);
    animation: line-scale 1s -.1s infinite cubic-bezier(.2, .68, .18, 1.08)
  }

  @-webkit-keyframes line-scale {
    0%, 100% {
      -webkit-transform: scaley(1);
      transform: scaley(1)
    }
    50% {
      -webkit-transform: scaley(.4);
      transform: scaley(.4)
    }
  }

  @keyframes line-scale {
    0%, 100% {
      -webkit-transform: scaley(1);
      transform: scaley(1)
    }
    50% {
      -webkit-transform: scaley(.4);
      transform: scaley(.4)
    }
  }

  .ball-scale-multiple {
    position: relative;
    -webkit-transform: translateY(30px);
    -ms-transform: translateY(30px);
    transform: translateY(30px);
    display: inline-block
  }

  .ball-scale-multiple > div {
    border-radius: 100%;
    animation-fill-mode: both;
    margin: 0;
    position: absolute;
    left: -30px;
    top: 0;
    opacity: 0;
    width: 50px;
    -webkit-animation: ball-scale-multiple 1s 0s linear infinite;
    animation: ball-scale-multiple 1s 0s linear infinite
  }

  .ball-scale-multiple > div:nth-child(2), .ball-scale-multiple > div:nth-child(3) {
    -webkit-animation-delay: -.2s;
    animation-delay: -.2s
  }

  @-webkit-keyframes ball-scale-multiple {
    0% {
      -webkit-transform: scale(0);
      transform: scale(0);
      opacity: 0
    }
    5% {
      opacity: 1
    }
    100% {
      -webkit-transform: scale(1);
      transform: scale(1);
      opacity: 0
    }
  }

  @keyframes ball-scale-multiple {
    0% {
      -webkit-transform: scale(0);
      transform: scale(0);
      opacity: 0
    }
    5% {
      opacity: 1
    }
    100% {
      -webkit-transform: scale(1);
      transform: scale(1);
      opacity: 0
    }
  }

  .ball-pulse-sync {
    display: inline-block
  }

  .ball-pulse-sync > div {
    width: 15px;
    height: 15px;
    border-radius: 100%;
    margin: 2px;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    display: inline-block
  }

  .ball-pulse-sync > div:nth-child(1) {
    -webkit-animation: ball-pulse-sync .6s -.21s infinite ease-in-out;
    animation: ball-pulse-sync .6s -.21s infinite ease-in-out
  }

  .ball-pulse-sync > div:nth-child(2) {
    -webkit-animation: ball-pulse-sync .6s -.14s infinite ease-in-out;
    animation: ball-pulse-sync .6s -.14s infinite ease-in-out
  }

  .ball-pulse-sync > div:nth-child(3) {
    -webkit-animation: ball-pulse-sync .6s -70ms infinite ease-in-out;
    animation: ball-pulse-sync .6s -70ms infinite ease-in-out
  }

  @-webkit-keyframes ball-pulse-sync {
    33% {
      -webkit-transform: translateY(10px);
      transform: translateY(10px)
    }
    66% {
      -webkit-transform: translateY(-10px);
      transform: translateY(-10px)
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0)
    }
  }

  @keyframes ball-pulse-sync {
    33% {
      -webkit-transform: translateY(10px);
      transform: translateY(10px)
    }
    66% {
      -webkit-transform: translateY(-10px);
      transform: translateY(-10px)
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0)
    }
  }

  .transparent-circle {
    display: inline-block;
    border-top: .5em solid rgba(255, 255, 255, .2);
    border-right: .5em solid rgba(255, 255, 255, .2);
    border-bottom: .5em solid rgba(255, 255, 255, .2);
    border-left: .5em solid #fff;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    -webkit-animation: transparent-circle 1.1s infinite linear;
    animation: transparent-circle 1.1s infinite linear;
    width: 50px;
    height: 50px;
    border-radius: 50%
  }

  .transparent-circle:after {
    border-radius: 50%;
    width: 10em;
    height: 10em
  }

  @-webkit-keyframes transparent-circle {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0)
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg)
    }
  }

  @keyframes transparent-circle {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0)
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg)
    }
  }

  .ball-spin-fade-loader {
    position: relative;
    top: -10px;
    left: -10px;
    display: inline-block
  }

  .ball-spin-fade-loader > div {
    width: 15px;
    height: 15px;
    border-radius: 100%;
    margin: 2px;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    position: absolute;
    -webkit-animation: ball-spin-fade-loader 1s infinite linear;
    animation: ball-spin-fade-loader 1s infinite linear
  }

  .ball-spin-fade-loader > div:nth-child(1) {
    top: 25px;
    left: 0;
    animation-delay: -.84s;
    -webkit-animation-delay: -.84s
  }

  .ball-spin-fade-loader > div:nth-child(2) {
    top: 17.05px;
    left: 17.05px;
    animation-delay: -.72s;
    -webkit-animation-delay: -.72s
  }

  .ball-spin-fade-loader > div:nth-child(3) {
    top: 0;
    left: 25px;
    animation-delay: -.6s;
    -webkit-animation-delay: -.6s
  }

  .ball-spin-fade-loader > div:nth-child(4) {
    top: -17.05px;
    left: 17.05px;
    animation-delay: -.48s;
    -webkit-animation-delay: -.48s
  }

  .ball-spin-fade-loader > div:nth-child(5) {
    top: -25px;
    left: 0;
    animation-delay: -.36s;
    -webkit-animation-delay: -.36s
  }

  .ball-spin-fade-loader > div:nth-child(6) {
    top: -17.05px;
    left: -17.05px;
    animation-delay: -.24s;
    -webkit-animation-delay: -.24s
  }

  .ball-spin-fade-loader > div:nth-child(7) {
    top: 0;
    left: -25px;
    animation-delay: -.12s;
    -webkit-animation-delay: -.12s
  }

  .ball-spin-fade-loader > div:nth-child(8) {
    top: 17.05px;
    left: -17.05px;
    animation-delay: 0s;
    -webkit-animation-delay: 0s
  }

  @-webkit-keyframes ball-spin-fade-loader {
    50% {
      opacity: .3;
      -webkit-transform: scale(.4);
      transform: scale(.4)
    }
    100% {
      opacity: 1;
      -webkit-transform: scale(1);
      transform: scale(1)
    }
  }

  @keyframes ball-spin-fade-loader {
    50% {
      opacity: .3;
      -webkit-transform: scale(.4);
      transform: scale(.4)
    }
    100% {
      opacity: 1;
      -webkit-transform: scale(1);
      transform: scale(1)
    }
  }</style>
  <link rel='dns-prefetch' href='//fonts.googleapis.com'/>
  <link rel='dns-prefetch' href='//s.w.org'/>
  <link rel="alternate" type="application/rss+xml" title="Fortuna Template - Jupiter WordPress Theme &raquo; Feed"
        href="http://demos.artbees.net/jupiter5/fortuna/feed/"/>
  <link rel="alternate" type="application/rss+xml"
        title="Fortuna Template - Jupiter WordPress Theme &raquo; Comments Feed"
        href="http://demos.artbees.net/jupiter5/fortuna/comments/feed/"/>

  <link rel="shortcut icon"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/2016/01/favicon.png"/>
  <script type="text/javascript">
    window.abb = {};
    php = {};
    window.PHP = {};
    PHP.ajax = "http://demos.artbees.net/jupiter5/fortuna/wp-admin/admin-ajax.php";
    PHP.wp_p_id = "67";
    var mk_header_parallax, mk_banner_parallax, mk_page_parallax, mk_footer_parallax, mk_body_parallax;
    var mk_images_dir = "http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/images",
        mk_theme_js_path = "http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js",
        mk_theme_dir = "http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter",
        mk_captcha_placeholder = "Enter Captcha",
        mk_captcha_invalid_txt = "Invalid. Try again.",
        mk_captcha_correct_txt = "Captcha correct.",
        mk_responsive_nav_width = 1200,
        mk_vertical_header_back = "Back",
        mk_vertical_header_anim = "1",
        mk_check_rtl = true,
        mk_grid_width = 1200,
        mk_ajax_search_option = "fullscreen_search",
        mk_preloader_bg_color = "#ffffff",
        mk_accent_color = "#1d161e",
        mk_go_to_top = "false",
        mk_smooth_scroll = "true",
        mk_preloader_bar_color = "#1d161e",
        mk_preloader_logo = "";
    var mk_header_parallax = false,
        mk_banner_parallax = false,
        mk_footer_parallax = false,
        mk_body_parallax = false,
        mk_no_more_posts = "No More Posts";
  </script>
  <link rel='stylesheet' id='contact-form-7-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.7'
        type='text/css' media='all'/>
  <link rel='stylesheet' id='theme-styles-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/min/core-styles.css?ver=5.9.1'
        type='text/css' media='all'/>
  <link rel='stylesheet' id='google-font-api-special-1-css'
        href='http://fonts.googleapis.com/css?family=Playfair+Display%3A100italic%2C200italic%2C300italic%2C400italic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic%2C100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.7.4'
        type='text/css' media='all'/>
  <link rel='stylesheet' id='js_composer_front-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/css/js_composer.min.css?ver=5.1.1'
        type='text/css' media='all'/>
  <link rel='stylesheet' id='global-assets-css-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/min/components-full.css?ver=5.9.1'
        type='text/css' media='all'/>
  <link rel='stylesheet' id='theme-options-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/mk_assets/theme-options-production.css?ver=1492671791'
        type='text/css' media='all'/>
  <link rel='stylesheet' id='mk-style-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/style.css?ver=4.7.4' type='text/css'
        media='all'/>
  <link rel='stylesheet' id='theme-dynamic-styles-css'
        href='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/custom.css?ver=4.7.4' type='text/css'
        media='all'/>
  <style id='theme-dynamic-styles-inline-css' type='text/css'>
    body {
      background-color: #fff;
    }

    .mk-header {
      background-color: #f7f7f7;
      background-size: cover;
      -webkit-background-size: cover;
      -moz-background-size: cover;
    }

    .mk-header-bg {
      background-color: #1d161e;
    }

    .mk-classic-nav-bg {
      background-color: #1d161e;
    }

    .master-holder-bg {
      background-color: #fff;
    }

    #mk-footer {
      background-color: #3d4045;
    }

    #mk-boxed-layout {
      -webkit-box-shadow: 0 0 0px rgba(0, 0, 0, 0);
      -moz-box-shadow: 0 0 0px rgba(0, 0, 0, 0);
      box-shadow: 0 0 0px rgba(0, 0, 0, 0);
    }

    .mk-news-tab .mk-tabs-tabs .is-active a, .mk-fancy-title.pattern-style span, .mk-fancy-title.pattern-style.color-gradient span:after, .page-bg-color {
      background-color: #fff;
    }

    .page-title {
      font-size: 20px;
      color: #4d4d4d;
      text-transform: uppercase;
      font-weight: 400;
      letter-spacing: 2px;
    }

    .page-subtitle {
      font-size: 14px;
      line-height: 100%;
      color: #a3a3a3;
      font-size: 14px;
      text-transform: none;
    }

    .header-style-1 .mk-header-padding-wrapper, .header-style-2 .mk-header-padding-wrapper, .header-style-3 .mk-header-padding-wrapper {
      padding-top: 91px;
    }

    @font-face {
      font-family: 'star';
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot');
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.eot?#iefix') format('embedded-opentype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.woff') format('woff'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.ttf') format('truetype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/star/font.svg#star') format('svg');
      font-weight: normal;
      font-style: normal;
    }

    @font-face {
      font-family: 'WooCommerce';
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot');
      src: url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.eot?#iefix') format('embedded-opentype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.woff') format('woff'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.ttf') format('truetype'), url('http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/stylesheet/fonts/woocommerce/font.svg#WooCommerce') format('svg');
      font-weight: normal;
      font-style: normal;
    }

    #mk-ornamental-title-3 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-3 .title {
      color: #ffffff;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-3 .title span::after, #mk-ornamental-title-3 .title span::before {
      border-top: 1px solid #ffffff;
    }

    #mk-ornamental-title-3 .title {
      font-size: 80px;
      line-height: 86px;
    }

    #mk-ornamental-title-3 .title span::after, #mk-ornamental-title-3 .title span::before {
      top: 42.5px;
    }

    #mk-shape-divider-4 .shape__container {
      background-color:;
    }

    #mk-shape-divider-4 .shape__container .shape {
      overflow: hidden;
      height:;
    }

    #mk-shape-divider-4 .shape__container .shape svg {
      position: relative;
      top: 0.6px;
    }

    #mk-shape-divider-4 .shape__container .shape .speech-left, #mk-shape-divider-4 .shape__container .shape .speech-right {
      background-color: #ffffff;
    }

    .full-width-2 .mk-video-color-mask {
      background: #e3aa7f;
      background: -moz-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e3aa7f), color-stop(100%, #26001f));
      background: -webkit-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: -o-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: -ms-linear-gradient(top, #e3aa7f 0%, #26001f 100%);
      background: linear-gradient(to bottom, #e3aa7f 0%, #26001f 100%);
    }

    .full-width-2 {
      min-height: 450px;
      margin-bottom: 0px;
    }

    .full-width-2 .page-section-content {
      padding: 0px 0 100px;
    }

    #background-layer--2 {;
      background-position: center top;
      background-repeat: no-repeat;;
    }

    #background-layer--2 .mk-color-layer {;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
    }

    .full-width-2 .mk-skip-to-next {
      bottom: 100px;
    }

    #padding-5 {
      height: 100px;
    }

    #fancy-title-6 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 35px;
      color: #c95a66;
      text-align: center;
      font-style: inherit;
      font-weight: 400;
      padding-top: 0px;
      padding-bottom: 0px;
    }

    #fancy-title-6 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-6 {
        text-align: center !important;
      }
    }

    #mk-ornamental-title-7 {
      margin-top: 0px;
      margin-bottom: 0px;
    }

    #mk-ornamental-title-7 .title {
      color: #888888;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-7 .title span::after, #mk-ornamental-title-7 .title span::before {
      border-top: 1px solid #888888;
    }

    #mk-ornamental-title-7 .title {
      font-size: 25px;
      line-height: 31px;
    }

    #mk-ornamental-title-7 .title span::after, #mk-ornamental-title-7 .title span::before {
      top: 15px;
    }

    #text-block-8 {
      margin-bottom: 0px;
      text-align: center;
    }

    #padding-9 {
      height: 50px;
    }

    #padding-10 {
      height: 100px;
    }

    #mk-shape-divider-12 .shape__container {
      background-color:;
    }

    #mk-shape-divider-12 .shape__container .shape {
      overflow: hidden;
      height:;
    }

    #mk-shape-divider-12 .shape__container .shape svg {
      position: relative;
      top: -0.6px;
    }

    #mk-shape-divider-12 .shape__container .shape .speech-left, #mk-shape-divider-12 .shape__container .shape .speech-right {
      background-color: #ffffff;
    }

    #padding-13 {
      height: 145px;
    }

    #mk-ornamental-title-15 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-15 .title {
      color: #222222;
      font-weight: inherit;
      font-style: inherit;
      text-transform: capitalize;
    }

    #mk-ornamental-title-15 .title span::after, #mk-ornamental-title-15 .title span::before {
      border-top: 1px solid #222222;
    }

    #mk-ornamental-title-15 .title {
      font-size: 35px;
      line-height: 41px;
    }

    #mk-ornamental-title-15 .title span::after, #mk-ornamental-title-15 .title span::before {
      top: 20px;
    }

    #fancy-title-16 {
      letter-spacing: 0px;
      text-transform: uppercase;
      font-size: 35px;
      color: #222222;
      text-align: center;
      font-style: inherit;
      font-weight: 400;
      padding-top: 0px;
      padding-bottom: px;
    }

    #fancy-title-16 span {
    }

    @media handheld, only screen and (max-width: 767px) {
      #fancy-title-16 {
        text-align: center !important;
      }
    }

    #text-block-17 {
      margin-bottom: 30px;
      text-align: center;
    }

    #mk-ornamental-title-18 {
      margin-top: 0px;
      margin-bottom: 20px;
    }

    #mk-ornamental-title-18 .title {
      color: #44c6d5;
      font-weight: bold;
      font-style: inherit;
      text-transform: uppercase;
    }

    #mk-ornamental-title-18 .title span::after, #mk-ornamental-title-18 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-18 .title {
      font-size: 14px;
      line-height: 20px;
    }

    #mk-ornamental-title-18 .title span::after {
      border-top: 1px solid #44c6d5;
    }

    #mk-ornamental-title-18 .title span::before {
      border-top: 1px solid #44c6d5;
    }

    #box-14 .box-holder {
      border: 5px solid #f5f1c4;
    }

    #box-14 .box-holder {
      background-color: rgba(255, 255, 255, 0.8);
    }

    #box-14 {
      margin-bottom: 0px;
    }

    #box-14 .box-holder {
      min-height: 100px;
      padding: 70px 60px;
    }

    #box-14 .box-holder:hover {
      background-color:;
    }

    #padding-19 {
      height: 40px;
    }

    .full-width-11 {
      min-height: 780px;
      margin-bottom: 0px;
    }

    .full-width-11 .page-section-content {
      padding: 100px 0 0px;
    }

    #background-layer--11 {;
      background-position: center top;
      background-repeat: no-repeat;;
    }

    #background-layer--11 .mk-color-layer {;
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
    }

    #wpcf7-f511-p143-o1 .vc_row . wpb_column:first-child {
      padding-right: 15px;
    }

    #wpcf7-f511-p143-o1 .vc_row . wpb_column:last-child {
      padding-left: 15px;
    }

    #wpcf7-f511-p143-o1 input[type=text] {
      border: 0;
      background-color: #aa8dd3;
      height: 40px;
      line-height: 40px;
      width: 100%;
      margin-bottom: 20px;
      font-size: 14px;
      color: #222222 !important;
    }

    #wpcf7-f511-p143-o1 label {
      font-size: 16px;
      margin-bottom: 10px;
      text-align: left;
    }

    #wpcf7-f511-p143-o1 input[type=file] {
      margin-bottom: 20px;
    }

    #wpcf7-f511-p143-o1 textarea {
      border: 0;
      background-color: #aa8dd3;
      height: 125px;
      width: 100%;
      margin-bottom: 10px;
      font-size: 14px;
      resize: none;
      Color: #222222 !important;
    }

    #wpcf7-f511-p143-o1 input[type=submit] {
      padding: 15px 40px;
      background-color: #00ffff;
      border: 0;
      color: #623e95;
      margin-top: 15px;
      font-size: 14px;
    }

    @media handheld, only screen and (max-width: 767px) {
      #wpcf7-f511-p143-o1 .vc_row . wpb_column:first-child {
        padding-right: 0 !important;
      }

      #wpcf7-f511-p143-o1 .vc_row . wpb_column:last-child {
        padding-left: 0 !important;
      }
    }
  </style>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
  <link rel='https://api.w.org/' href='http://demos.artbees.net/jupiter5/fortuna/wp-json/'/>
  <link rel="EditURI" type="application/rsd+xml" title="RSD"
        href="http://demos.artbees.net/jupiter5/fortuna/xmlrpc.php?rsd"/>
  <link rel="wlwmanifest" type="application/wlwmanifest+xml"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-includes/wlwmanifest.xml"/>
  <meta name="generator" content="WordPress 4.7.4"/>
  <link rel="canonical" href="http://demos.artbees.net/jupiter5/fortuna/reservation/"/>
  <link rel='shortlink' href='http://demos.artbees.net/jupiter5/fortuna/?p=67'/>
  <link rel="alternate" type="application/json+oembed"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemos.artbees.net%2Fjupiter5%2Ffortuna%2Freservation%2F"/>
  <link rel="alternate" type="text/xml+oembed"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fdemos.artbees.net%2Fjupiter5%2Ffortuna%2Freservation%2F&#038;format=xml"/>
  <script> var isTest = false; </script>
  <style id="js-media-query-css">.mk-event-countdown-ul:media( max-width: 750px ) li {
    width: 90%;
    display: block;
    margin: 0 auto 15px
  }

  .mk-process-steps:media( max-width: 960px ) ul:before {
    display: none !important
  }

  .mk-process-steps:media( max-width: 960px ) li {
    margin-bottom: 30px !important;
    width: 100% !important;
    text-align: center
  }</style>
  <meta itemprop="author" content=""/>
  <meta itemprop="datePublished" content="January 14, 2016"/>
  <meta itemprop="dateModified" content="January 15, 2016"/>
  <meta itemprop="publisher" content="Fortuna Template - Jupiter WordPress Theme"/>
  <style type="text/css">.recentcomments a {
    display: inline !important;
    padding: 0 !important;
    margin: 0 !important;
  }</style>
  <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
  <!--[if lte IE 9]>
  <link rel="stylesheet" type="text/css"
        href="http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/css/vc_lte_ie9.min.css"
        media="screen"><![endif]-->
  <meta name="generator" content="Jupiter 5.9.1"/>
  <noscript>
    <style type="text/css"> .wpb_animate_when_almost_visible {
      opacity: 1;
    }</style>
  </noscript>
  <link rel="stylesheet" href="main.css">

</head>

<body class="page-template-default page page-id-67 vertical-header-enabled vertical-header-left logo-align-center loading wpb-js-composer js-comp-ver-5.1.1 vc_responsive"
      itemscope="itemscope" itemtype="https://schema.org/WebPage" data-adminbar="">


<!-- Target for scroll anchors to achieve native browser bahaviour + possible enhancements like smooth scrolling -->
<div id="top-of-page"></div>

<div id="mk-boxed-layout">

  <div id="mk-theme-container">

      <?php include("header.php"); ?>

    <div id="theme-page" class="master-holder  clearfix" itemscope="itemscope" itemtype="https://schema.org/Blog">

      <div class="master-holder-bg-holder">
        <div id="theme-page-bg" class="master-holder-bg js-el"></div>
      </div>

      <div class="mk-main-wrapper-holder">

        <div id="mk-page-id-67" class="theme-page-wrapper mk-main-wrapper mk-grid full-layout no-padding ">
          <div class="theme-content no-padding" itemprop="mainEntityOfPage">


            <div class="clearboth"></div>
          </div>
        </div>
      </div>


      <div class="mk-page-section-wrapper">
        <div id="page-section-2"
             class="mk-page-section self-hosted   full_layout full-width-2 js-el js-master-row     center-y"
             data-intro-effect="false">


          <div class="mk-page-section-inner">


            <div style="background-color:#e3aa7f;opacity:0.8;" class="mk-video-color-mask"></div>


            <div class="background-layer-holder">
              <div id="background-layer--2" class="background-layer mk-background-stretch none-blend-effect js-el"
                   data-mk-img-set='{"landscape":{"external":"http://demos.artbees.net/jupiter5/fortuna/wp-content/uploads/sites/73/2016/01/reservation-bg.jpg"},"responsive":"true"}'>
                <div class="mk-color-layer"></div>
              </div>
            </div>

          </div>


          <div class="page-section-content vc_row-fluid mk-grid">
            <div class="mk-padding-wrapper">
              <div style="" class="vc_col-sm-12 wpb_column column_container  _ height-full">
                <div id="mk-ornamental-title-3"
                     class="mk-ornamental-title norman-short-single align-center title_as_text"><h2 class="title"><span
                        class="title-text">Rezervacija</span></h2>
                </div>
              </div>
            </div>
            <div class="clearboth"></div>
          </div>


          <div class="mk-shape-divider mk-shape-divider--stick speech-bottom-style big-size mk-shape-divider--stick-bottom"
               id="mk-shape-divider-4">
            <div class="shape__container">
              <div class="shape">

                <div class="speech-left"></div>
                <div class="speech-right"></div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>

          <div class="clearboth"></div>
        </div>
      </div>

      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
          <div class="theme-content no-padding">


          </div>
        </div>
      </div>

      <div class="wpb_row vc_row vc_row-fluid  mk-fullwidth-true  attched-false     js-master-row ">
        <div class="mk-grid">


          <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full">
          </div>

          <div style="" class="vc_col-sm-8 wpb_column column_container  _ height-full">

            <div id="padding-5" class="mk-padding-divider   clearfix"></div>


            <h2 id="fancy-title-6" class="mk-fancy-title  simple-style  color-single">
	<span>
				Rezervacijos forma		</span>
            </h2>
            <div class="clearboth"></div>


            <style>#mk-ornamental-title-7 {
              font-family: "Parisienne"
            }</style>
            <div id="mk-ornamental-title-7" class="mk-ornamental-title norman-short-single align-center title_as_text">
              <h2 class="title"><span class="title-text">&amp;</span></h2>
            </div>
            <div id="text-block-8" class="mk-text-block   ">


              <p>Quisque velit nisi pretium ut lacinia in elementum id enim sed porttitor lectus nibh donec rutrum
                congue leo eget malesuada orbi maximus nibh nec nisl rutrum eget maximus.</p>

              <div class="clearboth"></div>
            </div>

            <div id="padding-9" class="mk-padding-divider   clearfix"></div>

            <div role="form" class="wpcf7" id="wpcf7-f138-p67-o1" lang="en-US" dir="ltr">
              <div class="screen-reader-response"></div>
              <form action="/jupiter5/fortuna/reservation/#wpcf7-f138-p67-o1" method="post" class="wpcf7-form"
                    novalidate="novalidate">
                <div style="display: none;">
                  <input type="hidden" name="_wpcf7" value="138"/>
                  <input type="hidden" name="_wpcf7_version" value="4.7"/>
                  <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                  <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f138-p67-o1"/>
                  <input type="hidden" name="_wpnonce" value="934f73b4f6"/>
                </div>
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false">
                  <div class="vc_col-sm-6 wpb_column column_container ">
                    <label>Vardas</label><br/>
                    <span class="wpcf7-form-control-wrap text-51"><input type="text" name="text-51" value="" size="40"
                                                                         class="wpcf7-form-control wpcf7-text bc-input"
                                                                         aria-invalid="false"/></span>
                  </div>
                  <div class="vc_col-sm-6 wpb_column column_container ">
                    <label>Pavarde</label><br/>
                    <span class="wpcf7-form-control-wrap text-52"><input type="text" name="text-52" value="" size="40"
                                                                         class="wpcf7-form-control wpcf7-text bc-input"
                                                                         aria-invalid="false"/></span>
                  </div>
                </div>
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false">
                  <div class="vc_col-sm-6 wpb_column column_container ">
                    <label>Amžius</label><br/>
                    <span class="wpcf7-form-control-wrap text-53"><input type="text" name="text-53" value="" size="40"
                                                                         class="wpcf7-form-control wpcf7-text bc-input"
                                                                         aria-invalid="false"/></span>
                  </div>
                  <div class="vc_col-sm-6 wpb_column column_container ">
                    <label>Miestas</label><br/>
                    <span class="wpcf7-form-control-wrap text-54"><input type="text" name="text-54" value="" size="40"
                                                                         class="wpcf7-form-control wpcf7-text bc-input"
                                                                         aria-invalid="false"/></span>
                  </div>
                </div>
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false">
                  <div class="vc_col-sm-6 wpb_column column_container ">
                    <label>Telefonas</label><br/>
                    <span class="wpcf7-form-control-wrap text-53"><input type="text" name="text-53" value="" size="40"
                                                                         class="wpcf7-form-control wpcf7-text bc-input"
                                                                         aria-invalid="false"/></span>
                  </div>
                  <div class="vc_col-sm-6 wpb_column column_container ">
                    <label>El.paštas</label><br/>
                    <span class="wpcf7-form-control-wrap text-54"><input type="text" name="text-54" value="" size="40"
                                                                         class="wpcf7-form-control wpcf7-text bc-input"
                                                                         aria-invalid="false"/></span>
                  </div>
                </div>
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false">
                  <div class="vc_col-sm-12 wpb_column column_container ">
                    <label>Klausimai</label><br/>
                    <span class="wpcf7-form-control-wrap textarea-586"><textarea name="textarea-586" cols="40" rows="10"
                                                                                 class="wpcf7-form-control wpcf7-textarea bc-textarea"
                                                                                 aria-invalid="false"></textarea></span>
                  </div>
                </div>
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-true attched-false">
                  <input type="submit" value="Siųsti" class="wpcf7-form-control wpcf7-submit"/>
                </div>
                <div class="wpcf7-response-output wpcf7-display-none"></div>
              </form>
            </div>
            <div id="padding-10" class="mk-padding-divider   clearfix"></div>

          </div>

          <div style="" class="vc_col-sm-2 wpb_column column_container  _ height-full">
          </div>

        </div>
      </div>


      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper full-layout mk-grid vc_row-fluid no-padding">
          <div class="theme-content no-padding">


            <div class="clearboth"></div>
          </div>
        </div>
      </div>


      <div class="mk-page-section-wrapper">
        <div id="page-section-11"
             class="mk-page-section self-hosted   full_layout full-width-11 js-el js-master-row    "
             data-intro-effect="false">


          <div class="mk-shape-divider mk-shape-divider--stick speech-top-style big-size mk-shape-divider--stick-top"
               id="mk-shape-divider-12">
            <div class="shape__container">
              <div class="shape">

                <div class="speech-left"></div>
                <div class="speech-right"></div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>




          <div class="clearboth"></div>
        </div>
      </div>

      <div class="mk-main-wrapper-holder">
        <div class="theme-page-wrapper no-padding full-layout mk-grid vc_row-fluid">
          <div class="theme-content no-padding">


            <div class="clearboth"></div>

            <div class="clearboth"></div>
          </div>
          <div class="clearboth"></div>

        </div>
      </div>


    </div>

    <section id="mk-footer-unfold-spacer"></section>

    <section id="mk-footer" class=" mk-footer-disable" role="contentinfo" itemscope="itemscope"
             itemtype="https://schema.org/WPFooter">
    </section>
  </div>
</div>

<div class="bottom-corner-btns js-bottom-corner-btns">
  <div class="mk-quick-contact-wrapper  js-bottom-corner-btn js-bottom-corner-btn--contact">

    <a href="#" class="mk-quick-contact-link">
      <svg class="mk-svg-icon" data-name="mk-icon-envelope" data-cacheid="icon-59075ca6f27c6"
           style=" height:20px; width: 20px; " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792">
        <path d="M1792 710v794q0 66-47 113t-113 47h-1472q-66 0-113-47t-47-113v-794q44 49 101 87 362 246 497 345 57 42 92.5 65.5t94.5 48 110 24.5h2q51 0 110-24.5t94.5-48 92.5-65.5q170-123 498-345 57-39 100-87zm0-294q0 79-49 151t-122 123q-376 261-468 325-10 7-42.5 30.5t-54 38-52 32.5-57.5 27-50 9h-2q-23 0-50-9t-57.5-27-52-32.5-54-38-42.5-30.5q-91-64-262-182.5t-205-142.5q-62-42-117-115.5t-55-136.5q0-78 41.5-130t118.5-52h1472q65 0 112.5 47t47.5 113z"/>
      </svg>
    </a>
    <div id="mk-quick-contact">
      <div class="mk-quick-contact-title">Contact Us</div>
      <p>We're not around right now. But you can send us an email and we'll get back to you, asap.</p>
      <form class="mk-contact-form" method="post" novalidate="novalidate">
        <input type="text" placeholder="Name*" required="required" id="name" name="name" class="text-input" value=""
               tabindex="166"/>
        <input type="email" data-type="email" required="required" placeholder="Email*" id="email" name="email"
               class="text-input" value="" tabindex="167"/>
        <textarea placeholder="Message*" required="required" id="content" name="content" class="textarea"
                  tabindex="168"></textarea>

        <div class="btn-cont">
          <button tabindex="169" class="mk-progress-button mk-contact-button accent-bg-color button"
                  data-style="move-up">
            <span class="mk-progress-button-content">Siųsti</span>
            <span class="mk-progress">
                            <span class="mk-progress-inner"></span>
                        </span>
            <span class="state-success"><svg class="mk-svg-icon" data-name="mk-moon-checkmark"
                                             data-cacheid="icon-59075ca6f2c2c" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 512 512"><path
                    d="M432 64l-240 240-112-112-80 80 192 192 320-320z"/></svg></span>
            <span class="state-error"><svg class="mk-svg-icon" data-name="mk-moon-close"
                                           data-cacheid="icon-59075ca6f2f18" xmlns="http://www.w3.org/2000/svg"
                                           viewBox="0 0 512 512"><path
                    d="M507.331 411.33l-.006-.005-155.322-155.325 155.322-155.325.006-.005c1.672-1.673 2.881-3.627 3.656-5.708 2.123-5.688.912-12.341-3.662-16.915l-73.373-73.373c-4.574-4.573-11.225-5.783-16.914-3.66-2.08.775-4.035 1.984-5.709 3.655l-.004.005-155.324 155.326-155.324-155.325-.005-.005c-1.673-1.671-3.627-2.88-5.707-3.655-5.69-2.124-12.341-.913-16.915 3.66l-73.374 73.374c-4.574 4.574-5.784 11.226-3.661 16.914.776 2.08 1.985 4.036 3.656 5.708l.005.005 155.325 155.324-155.325 155.326-.004.005c-1.671 1.673-2.88 3.627-3.657 5.707-2.124 5.688-.913 12.341 3.661 16.915l73.374 73.373c4.575 4.574 11.226 5.784 16.915 3.661 2.08-.776 4.035-1.985 5.708-3.656l.005-.005 155.324-155.325 155.324 155.325.006.004c1.674 1.672 3.627 2.881 5.707 3.657 5.689 2.123 12.342.913 16.914-3.661l73.373-73.374c4.574-4.574 5.785-11.227 3.662-16.915-.776-2.08-1.985-4.034-3.657-5.707z"/></svg></span>
          </button>
        </div>
        <input type="hidden" id="security" name="security" value="f477bf6287"/><input type="hidden"
                                                                                      name="_wp_http_referer"
                                                                                      value="/jupiter5/fortuna/reservation/"/>
        <input type="hidden" id="sh_id" name="sh_id" value="15"><input type="hidden" id="p_id" name="p_id" value="2342">
        <div class="contact-form-message clearfix"></div>
      </form>
      <div class="bottom-arrow"></div>
    </div>
  </div>
</div>


<div class="mk-fullscreen-search-overlay">
  <a href="#" class="mk-fullscreen-close">
    <svg class="mk-svg-icon" data-name="mk-moon-close-2" data-cacheid="icon-59075ca6f3319"
         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
      <path d="M390.628 345.372l-45.256 45.256-89.372-89.373-89.373 89.372-45.255-45.255 89.373-89.372-89.372-89.373 45.254-45.254 89.373 89.372 89.372-89.373 45.256 45.255-89.373 89.373 89.373 89.372z"/>
    </svg>
  </a>
  <div class="mk-fullscreen-search-wrapper">
    <p>Start typing and press Enter to search</p>
    <form method="get" id="mk-fullscreen-searchform" action="http://demos.artbees.net/jupiter5/fortuna/">
      <input type="text" value="" name="s" id="mk-fullscreen-search-input"/>
      <i class="fullscreen-search-icon">
        <svg class="mk-svg-icon" data-name="mk-icon-search" data-cacheid="icon-59075ca6f355e"
             style=" height:25px; width: 23.214285714286px; " xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 1664 1792">
          <path d="M1152 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/>
        </svg>
      </i>
    </form>
  </div>
</div>


<footer id="mk_page_footer">
  <script type="text/javascript">
    php = {
      hasAdminbar: false,
      json: (null != null) ? null : "",
      jsPath: 'http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js'
    };
  </script>
  <link rel='stylesheet' id='Parisienne-css'
        href='//fonts.googleapis.com/css?family=Parisienne%3A100italic%2C200italic%2C300italic%2C400italic%2C500italic%2C600italic%2C700italic%2C800italic%2C900italic%2C100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.7.4'
        type='text/css' media='all'/>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"};
    /* ]]> */
  </script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.7'></script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js/plugins/wp-enqueue/smoothscroll.js?ver=5.9.1'></script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/comment-reply.min.js?ver=4.7.4'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var ajax_login_object = {
      "ajaxurl": "http:\/\/demos.artbees.net\/jupiter5\/fortuna\/wp-admin\/admin-ajax.php",
      "redirecturl": "http:\/\/demos.artbees.net\/jupiter5\/fortuna\/reservation\/",
      "loadingmessage": "Sending user info, please wait..."
    };
    /* ]]> */
  </script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js/core-scripts.js?ver=5.9.1'></script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-includes/js/wp-embed.min.js?ver=4.7.4'></script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-content/plugins/js_composer_theme/assets/js/dist/js_composer_front.min.js?ver=5.1.1'></script>
  <script type='text/javascript'
          src='http://demos.artbees.net/jupiter5/fortuna/wp-content/themes/jupiter/assets/js/min/components-full.js?ver=5.9.1'></script>
  <script type="text/javascript">
  </script>
  <script type="text/javascript">    window.get = {};
  window.get.captcha = function (enteredCaptcha) {
    return jQuery.get(ajaxurl, {action: "mk_validate_captcha_input", captcha: enteredCaptcha});
  };</script>
  <script>
    // Run this very early after DOM is ready
    (function ($) {
      // Prevent browser native behaviour of jumping to anchor
      // while preserving support for current links (shared across net or internally on page)
      var loc = window.location,
          hash = loc.hash;

      // Detect hashlink and change it's name with !loading appendix
      if (hash.length && hash.substring(1).length) {
        var $topLevelSections = $('.vc_row, .mk-main-wrapper-holder, .mk-page-section, #comments');
        var $section = $topLevelSections.filter('#' + hash.substring(1));
        // We smooth scroll only to page section and rows where we define our anchors.
        // This should prevent conflict with third party plugins relying on hash
        if (!$section.length)  return;
        // Mutate hash for some good reason - crazy jumps of browser. We want really smooth scroll on load
        // Discard loading state if it already exists in url (multiple refresh)
        hash = hash.replace('!loading', '');
        var newUrl = hash + '!loading';
        loc.hash = newUrl;
      }
    }(jQuery));
  </script>


</footer>
</body>
</html>